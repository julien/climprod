/**
 * Titre : Climprod<p>
 */
package fr.ird.climprod;

import java.util.StringTokenizer;
import java.util.Hashtable;
import java.util.HashMap;
import java.util.Vector;
import java.io.InputStream;

public class QuestionReponse {

    private static int nbData = 0;
    private static int[] rTrue;
    private static int[] rFalse;
    private static int[] regle;
    private static String[] script;
    private static String[] help;
    private static String[] name;
    private static String[][] item;
    private static HashMap<String, String> htNum = new HashMap<String, String>();
    private static HashMap<String, String> htName = new HashMap<String, String>();
    private static int[] type;
    private static int indexEnCours;                 //Index associ� � la cl� (Num�ro de r�gle)
    private static int numEnCours;                   //Num�ro de r�gle
    private static String scriptEnCours;
    private static String[] itemEnCours;
    private static String helpEnCours;
    private static int[] selectedItem;
    private static Vector<Integer> raisonnement = new Vector<Integer>();
    //private static int etapeEnCours=0;


    /*
     Assure la lecture du fichier des questions et initialise l'ensemble des variables.
     @param un String repr�sentant le nom "compl�t" du fichier arbres.cvs.
     */
    public static void initScript(InputStream file) // public static void initScript(String file)
    {
        try {

            String[] dataLine;
            String tt;
            ReadFileText rft = new ReadFileText(file);
            dataLine = rft.getLines();
            nbData = dataLine.length;
            rTrue = new int[nbData];
            rFalse = new int[nbData];
            regle = new int[nbData];
            script = new String[nbData];
            help = new String[nbData];
            name = new String[nbData];
            item = new String[nbData][];
            type = new int[nbData];
            selectedItem = new int[nbData];

            for (int i = 0; i < dataLine.length; i++) {
                StringTokenizer d = new StringTokenizer(dataLine[i], ";");
                tt = d.nextToken().trim();
                htNum.put(tt, Integer.toString(i));
                //htNum.put(d.nextToken().trim(),Integer.toString(i));
                rTrue[i] = Integer.parseInt(d.nextToken().trim());
                rFalse[i] = Integer.parseInt(d.nextToken().trim());
                regle[i] = Integer.parseInt(d.nextToken().trim());
                type[i] = Integer.parseInt(d.nextToken().trim());
                int k = d.countTokens();
                if (k != 0) {
                    name[i] = d.nextToken().trim();
                    htName.put(name[i], Integer.toString(i));
                    // Global.htVar.put(name[i],"-1");
                    script[i] = d.nextToken().trim();
                    help[i] = d.nextToken().trim().toUpperCase();

                    k = d.countTokens();
                    if (k != 0) {
                        item[i] = new String[k];
                        for (int j = 0; j < k; j++) {
                            item[i][j] = d.nextToken().trim();
                        }
                    } else {
                        k = 3;
                        item[i] = new String[3];
                        item[i][0] = "Yes";
                        item[i][1] = "No";
                        item[i][2] = "Don't know";
                    }
                } else {
                    name[i] = null;
                    script[i] = null;
                    help[i] = null;
                    item[i] = null;
                }
                /*System.out.println(i+"  " +tt+"  "+  regle[i]);
                 for(int l=0;l<k;l++)
                 System.out.print(item[i][l]+"  ");
                 System.out.println("******************");*/
            }
            activeDidacticiel(true);
        } catch (Exception e) {
            new MsgDialogBox(0, e.getMessage(), 0, Global.CadreMain);
        }
    }

    /*
     "Charge" la question en cours.
     */
    public static void loadQuestion() {
        //numEnCours=Integer.parseInt((String)htNum.get(number));

        if (numEnCours >= 0) {
            scriptEnCours = script[indexEnCours];
            if (script[indexEnCours] != null) {
                itemEnCours = new String[item[indexEnCours].length];
                for (int i = 0; i < itemEnCours.length; i++) {
                    itemEnCours[i] = item[indexEnCours][i];
                }
            } else {
                itemEnCours = null;
            }
            helpEnCours = help[indexEnCours];

        } else {
            scriptEnCours = null;
            itemEnCours = null;
            helpEnCours = null;
        }

    }


    /*
     Donne le num�ro de question en cours
     @return un entier le num�ro de question
     */
    public static int getNum() {
        return numEnCours;
    }


    /*
     Donne le num�ro de r�gle  associ� � la question.
     @return un entier le num�ro de r�gle, ou -1 si pas de question associ�e.
     */
    public static int getNumRegle() {
        if (numEnCours >= 0) {
            return regle[indexEnCours];
        } else {
            return -1;
        }
    }


    /*
     Donne le script associ� � la question.
     @return un String ou null si pas de script.
     */
    public static String getScript() {

        return scriptEnCours;
    }


    /*
     Donne les items r�ponse associ�s � la question.
     @return un String[], la liste des items ou null si d'items.
     */
    public static String[] getItem() {

        return itemEnCours;
    }


    /*
     Donne le nom du fichier d'aide associ� � la question.
     @return un String ou null si pas de fichier.
     */
    public static String getHelp() {

        return helpEnCours;
    }


    /*
     Donne le num�ro de l'item s�lectionn� en r�ponse � la question.
     @return un int ou -1 si pas de réponse valide.
     */
    public static int getReponse() {
        if (numEnCours >= 0) {
            return selectedItem[indexEnCours];
        } else {
            return -1;
        }
    }


    /*
    Validate the answer number to the current question and the potential corresponding rule
    This method is used for questions which verify a rule directly without any item
    @param none
     */
    public static void setReponse() {
        boolean result = false;
        if (regle[indexEnCours] != -1) {
            result = TexteRegles.isTrue();
        }
        if (result) {
            selectedItem[indexEnCours] = 0;
        } else {
            selectedItem[indexEnCours] = 1;
        }
    }

    /*
    Validate the answer number to the current question and the potential corresponding rule
    @param int index which represents the selected answer item number to the question
     */
    public static void setReponse(int index, String reptxt, String questxt, String commentTxt) {
        selectedItem[indexEnCours] = index;
        index = index + 1;
        System.out.println("Answer to rule " + regle[indexEnCours] + " given to question " + numEnCours + " is  " + index);

        // build list questions for html
        //System.out.println("AAA question number : "+numEnCours);
        //System.out.println("AAA question : "+questxt);
        //System.out.println("BBB i guess answer is : "+reptxt);
        Global.questionDic.put(numEnCours, questxt);
        Global.answerDic.put(numEnCours, reptxt);
        //System.out.println(numEnCours+ " || "+questxt+" || "+reptxt+" || "+commentTxt);
        Global.warningDic.put(numEnCours, commentTxt);

        switch (numEnCours) {
            case 1:
                Global.changement_exploitation = index;
                break;
            case 2:
                Global.unite_standardisee = index;
                break;
            case 4:
                Global.effet_delais_abundance_negligeable = index;
                break;
            case 5:
                Global.stock_unique = index;
                break;
            case 6:
                Global.sous_stock = index;
                break;
            case 7:
                Global.sous_stock_isole = index;
                break;
            case 9:
                Global.under_and_over_exploited = index;
                break;
            case 10:
                Global.under_and_optimaly = index;
                break;
            case 11:
                Global.statistiques_anormales = index;
                break;
            case 12:
                Global.unstability = index;
                break;
            case 13:
                Global.abnormal_points_dist = index;
                break;
            case 14:
                Global.abnormal_points_scatt = index;
                break;
            case 15:
                Global.effort_increasing = index;
                break;
            case 16:
                Global.independance = index;
                break;
            case 17:
                Global.nb_classes_exploitees = index;
                Data.init_val();//Pour obtenir la relation en fct du d�calage
                break;
            case 18:
                Global.relationCPU_E = 0; //Initialisations pour prendre
                Global.relationCPU_V = 0;   //en compte le changement
                Global.recruitment_age = 0;    //  de la pr�pond�rence
                Global.begin_influence_period = 0;  //de l'effort ou de l'environement
                Global.end_influence_period = 0;
                Global.effort_preponderant = index;
                if (index == 3) {
                    index = index - 1;
                }
                Global.typeModele = index;
                break;
            case 19:

                Global.decreasing_relationship = index;
                break;
            case 20:
            case 69:
                // Global.relationCPU_E=index;
                if (index == 1) {
                    Global.relationCPU_E = RechercheModele.lineaire;
                } else {
                    Global.relationCPU_E = RechercheModele.exponentiel;
                }
                break;
            case 21:
            case 70:
                Global.pessimiste = index;
                break;
            case 22:
            case 71:
                Global.stock_deja_effondre = index;
                break;
            case 23:
            case 72:
                Global.lifespan = index;
                //System.out.println("lifespan " +Global.lifespan  );
                break;
            case 24:
            case 73:
                Global.rapport_vie_exploitee_inferieur_deux = index;
                break;
            case 25:
            case 74:
                Global.stock_divise = index;
                break;
            case 26:
            case 75:
                Global.reserves_naturelles = index;
                break;
            case 27:
            case 76:
                Global.premiere_reproduction_avant_recrutement = index;
                break;
            case 28:
            case 77:
                Global.fecondite_faible = index;
                break;
            case 29:
            case 78:
                Global.cpue_unstable = index;
                break;
            case 47:
            case 67:
                //String[] influence={"abundance","catchability","both"};//probl�me du both
                Global.environmental_influence = Global.influenceEnv[index];
                Global.typeModele = RechercheModele.mixte;
                break;
            case 48:
            case 68:
                Global.cpue_sous_sur_production = index;
                break;
            case 49:
            case 51:
            case 60:
                Global.linear_relationship = index;
                if (index == 1) {
                    Global.relationCPU_V = RechercheModele.lineaire;
                }

                break;
            case 50:
            case 61:
                Global.monotonic_relationship = index;
                if (index == 1) {
                    Global.relationCPU_V = RechercheModele.general;
                } else {
                    Global.relationCPU_V = RechercheModele.quadratique;
                }
                break;

            case 52:
            case 57:
                Global.recruitment_age = index;
                break;
            case 53:
            case 58:
                Global.begin_influence_period = index - 1;
                break;
            case 54:
            case 59:
                Global.end_influence_period = index - 1;
                Data.init_val();
                break;
            case 97:
                Global.good_results = index;
                break;
            case 98:
                Global.trend_residuals = index;
                break;
            case 100:
                Global.coeff_determination_instable = index;
                break;

        }

        if (regle[indexEnCours] != -1) {
            TexteRegles.isTrue();
        }

    }

    /*
     Decide what is next question
     @param int number index of the current answer
     */
    public static void nextQuestion(int index) {
        raisonnement.add(numEnCours);
        //indexEnCours=Integer.parseInt((String)htNum.get(Integer.toString(numEnCours)));
        // System.out.println(indexEnCours+ "    "+numEnCours );
        if (index == 0) //MODIF ICI 0
        {
            numEnCours = rTrue[indexEnCours];
        } else {
            numEnCours = rFalse[indexEnCours];
        }
        System.out.println("La question en cours a le numéro :" + numEnCours);

        Object tp$ = htNum.get(Integer.toString(numEnCours));
        if (tp$ != null) {
            indexEnCours = Integer.parseInt((String) tp$);
        } else {
            indexEnCours = -1;
        }

        System.out.println("IndexEnCours: " + indexEnCours);
    }

    /*
     Go back to previous question
     */
    public static int previousQuestion() {
        int k = raisonnement.size();
        if (k == 0) {
            return k;
        }
        // int etapeActuelle=QuestionReponse.getEtape();
        numEnCours = ((Integer) (raisonnement.lastElement()));
        indexEnCours = Integer.parseInt((String) htNum.get(Integer.toString(numEnCours)));

        //System.out.println("previous" + numEnCours);
        raisonnement.removeElementAt(k - 1);
        //System.out.println("Size" + raisonnement.size());
        return k - 1;
    }

    public static int getEtape() {
        if (indexEnCours >= 0) {
            //System.out.println("regle " + QuestionReponse.getNumRegle()+ " Etape " + type[indexEnCours] );
            return type[indexEnCours];
        } else {
            return -1;
        }

    }

    /*
     Init all answers to -1
     */
    public static void reset() {
        raisonnement.clear();
        for (int i = 0; i < selectedItem.length; i++) {
            selectedItem[i] = -1;
        }
        numEnCours = 0;
        indexEnCours = Integer.parseInt((String) htNum.get(Integer.toString(numEnCours)));
    }

    /*
     Return item for question named nameVar
     No control on nameVar
     (Used to get params of a specific question)
     */
    public static String[] getListScript() {

        return script;
    }

    public static String[] getItemQuestion(String nameVar) {
        int it = Integer.parseInt((String) htName.get(nameVar));
        if (script[it] != null) {
            String[] itemQuestion = new String[item[it].length];
            for (int i = 0; i < itemQuestion.length; i++) {
                itemQuestion[i] = item[it][i];
            }
            return itemQuestion;
        } else {
            return null;
        }
    }


    /*
     Activate tutorial
     @param boolean activate if true
     */
    public static void activeDidacticiel(boolean active) {
        reset();
    }

    /*
     Question to display on screen
     @return boolean
     */
    public static boolean displayQuestion() {
        String tc = TexteRegles.getComment();
        if (scriptEnCours != null) {
            return true;
        } else return (type[indexEnCours] == Global.fixationM && (!(tc == null) && !tc.equals("")));

    }

    /*
     Return number of already asked questions
     @return un entier  .
     */
    public static int getNbQuestion() {
        return raisonnement.size();

    }

}
