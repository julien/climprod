
/**
 * Titre : Climprod<p>
 * Lecture d'un fichier au format texte.
 */
package fr.ird.climprod;

import java.io.*;
import java.util.*;
public class ReadFileText{
	public  ReadFileText(InputStream in)throws OnError{
	 	try{
                    BufferedReader buffile=new BufferedReader(new InputStreamReader(in));
	 	    ReadLines(buffile);
                    buffile.close();
 	 	}
		catch (IOException e){
                    throw new OnError(e.getMessage());
		}
	}
	public  ReadFileText(String fileName)throws OnError{
	 	try{
              	    BufferedReader buffile=new BufferedReader(new FileReader(fileName));
	 	    ReadLines(buffile);
                    buffile.close();
 	 	}
		catch (IOException e){
                    throw new OnError(e.getMessage());
		}
	}
	protected void ReadLines(BufferedReader buf) throws IOException{
		String s;
		Vector<String> v=new Vector<String>();
		while ((	s=buf.readLine())!=null){
			v.add(s);
		}
	        lines=new String[v.size()];
		v.copyInto(lines);
		ok=true;
	}
	public String[] getLines(){
                return lines;

	}
	private String[] lines;
	private boolean ok;
}