/**
 * Titre : Climprod<p>
 */
package fr.ird.climprod;

import java.awt.*;
import javax.swing.*;

public class UtilCadre {

    static public void Centrer(JFrame f) {
        GraphicsDevice gd = Global.CadreMain.getGraphicsConfiguration().getDevice();
        int xscr = gd.getDisplayMode().getWidth();
        int yscr = gd.getDisplayMode().getHeight();
        int xoffset = gd.getConfigurations()[0].getBounds().x;
        int yoffset = gd.getConfigurations()[0].getBounds().y;
        //out.println("Centrer offset: "+xoffset+" "+yoffset);
        Dimension size = f.getSize();

        int y = (yscr - size.height) / 2;
        int x = (xscr - size.width) / 2;
        f.setLocation(x+xoffset, y+yoffset);
    }

    static public void Centrer(JDialog f) {
        GraphicsDevice gd = Global.CadreMain.getGraphicsConfiguration().getDevice();
        int xscr = gd.getDisplayMode().getWidth();
        int yscr = gd.getDisplayMode().getHeight();
        int xoffset = gd.getConfigurations()[0].getBounds().x;
        int yoffset = gd.getConfigurations()[0].getBounds().y;

        //Dimension screenSize = f.getToolkit().getScreenSize();
        Dimension size = f.getSize();
        int y = (yscr - size.height) / 2;
        int x = (xscr - size.width) / 2;
        f.setLocation(x+xoffset, y+yoffset);
    }

    /*
     Positionne un  controle Frame en bas � droite
     @param JFrame le Frame � positionner
     */
    static public void bottomRight(JFrame f) {
        Dimension screenSize = f.getToolkit().getScreenSize();
        Dimension size = f.getSize();
        int y = (screenSize.height - size.height);
        int x = (screenSize.width - size.width);
        f.setLocation(x, y);
    }

    static public void Size(JFrame f, int sx, int sy) {
    		//Dimensionne le controle en % de taille de l'�cran
        //sx et sy doivent �tre compris entre 10 et 100
        if ((sx > 100) || (sx < 10)) {
            sx = 100;
        }
        if ((sy > 100) || (sy < 10)) {
            sy = 100;
        }

        //Dimension screenSize = f.getToolkit().getScreenSize();
        //int y = (screenSize.height * sy/100);
        //int x = (screenSize.width * sx/100);
        //GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
        GraphicsDevice gd = Global.CadreMain.getGraphicsConfiguration().getDevice();
        /*for (int aa=0; aa<gd.getConfigurations().length; aa++){
            System.out.println("BOUNDS : "+gd.getConfigurations()[aa].getBounds());
        }*/
        int x = gd.getDisplayMode().getWidth() * sx / 100;
        int y = gd.getDisplayMode().getHeight() * sy / 100;

        f.setSize(x, y);
    }

    static public void Size(JDialog f, int sx, int sy, boolean screen) {
    		//Dimensionne le controle en % de taille du container parent
        //screen false ou de l'�cran screen true
        //sx et sy doivent �tre compris entre 10 et 100
        if ((sx > 100) || (sx < 10)) {
            sx = 100;
        }
        if ((sy > 100) || (sy < 10)) {
            sy = 100;
        }
        Dimension dialogSize = null;
        GraphicsDevice gd = Global.CadreMain.getGraphicsConfiguration().getDevice();
        
        int xscr = gd.getDisplayMode().getWidth();
        int yscr = gd.getDisplayMode().getHeight();
        
        /*if (screen) {
            dialogSize = f.getToolkit().getScreenSize();

        } else {
            dialogSize = f.getParent().getSize();
        }*/
        // Dimension
        int y = (yscr * sy / 100);
        int x = (yscr * sx / 100);
        f.setSize(x, y);
    }

    static public void leftResize(JFrame f, int w, int h) {
        Size(f, w, h);
        
        GraphicsDevice gd = Global.CadreMain.getGraphicsConfiguration().getDevice();
        int xoffset = gd.getConfigurations()[0].getBounds().x;
        int yoffset = gd.getConfigurations()[0].getBounds().y;
        //System.out.println(xoffset+" "+yoffset);
        f.setLocation(0 + xoffset, 0 + yoffset);
        f.repaint();
    }

    static public void rightResize(JFrame f, int w, int h) {
        Size(f, w, h);

        //Dimension screenSize = f.getToolkit().getScreenSize();
        //f.setLocation(screenSize.width - size.width, 0);
        Dimension size = f.getSize();
        GraphicsDevice gd = Global.CadreMain.getGraphicsConfiguration().getDevice();
        int xoffset = gd.getConfigurations()[0].getBounds().x;
        int yoffset = gd.getConfigurations()[0].getBounds().y;
        //GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();

        int width = gd.getDisplayMode().getWidth();

        f.setLocation((width - size.width)+xoffset, 0+yoffset);
    }

}
