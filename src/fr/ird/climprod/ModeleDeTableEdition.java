package fr.ird.climprod;

import javax.swing.table.*;


/**
 * <p>Titre : </p>
 * <p>Description : </p>
 * <p>Copyright : Copyright (c) 2006</p>
 * <p>Soci�t� : </p>
 * @author non attribu�
 * @version 1.0
 */

public class ModeleDeTableEdition extends AbstractTableModel  {

  /**
  * Mod�le de table g�rant l'affichage dans jTabData
  */



   public ModeleDeTableEdition(Object[][]dataTable,String[] col) {
     columnNames = col;
     data = dataTable;
     changement=false;

   }

   public int getColumnCount() {
    return columnNames.length;
   }

   public Object getValueAt(int row, int col) {
     return data[row][col];
   }

   public int getRowCount() {
     return data.length;
   }

   public boolean isCellEditable(int row, int col) {
            return true;
   }


 public String getColumnName(int col) {
         return columnNames[col];
     }

 public Class getColumnClass(int c) {
    return getValueAt(0, c).getClass();
 }

    public void setValueAt(Object value, int row, int col) {
       String res=value.toString().trim();
       String message="";
       if(res.length()!=0){
         data[row][col] =res.toString();
         fireTableCellUpdated(row, col);
         changement=true;
       }
       else{

         if (col==0)
           message="Only integer values are allowed for \"Years\" column !";
         else
           message="Only numericals values are allowed !";

         MsgDialogBox msg=new MsgDialogBox(0,message,0, Global.CadreMain);

       }

 }

 public boolean isChanged(){
   return changement;
 }
 public void updateIsSave(){
   changement=false;
 }

 final String[] columnNames ;
 final Object[][] data ;
 private boolean changement;

}