/**
 * Titre : Climprod<p>
 *
 *
 */
package fr.ird.climprod;

import static fr.ird.climprod.Data.getNbDataRetenue;
import java.text.DecimalFormat;
import java.awt.Color;

public class Modele {

    static final private int nparmax = 4;
    static final private int maxrit = 100;

    static final private double rl = 1.5;
    static final private double alpha = 4;
    static final private double eps = 1.E-8;
    static final private double mul = 1.E-3;
    static private int nim;
    static private double lambda;
    static private int nbre_par;
    static private int riter, iter, ritertot;
    static private double val_init, val_fin, ecart;
    static private double r, sompar, variance_pue, yy, aic;

    static private double[] ptmp;
    static private double[][] covar;

    static private double[][] hessien;
    static private double[] covpy;
    static private double[] par_alors;
    static private double[] racineh;
    static private double[] derivee;
    static private double[] delta;
    static private double[] par_init;
    static private double[] par_inter;
    static private double[] spar;
    static private double[] par;
    static private double[] dpar;
    static private double[] sompari;
    static private double[] f, v, vbar, pue, res, fittedpue;
    static private double[] resiter;
    static private double[][] derfonc;

    public static void Estimer() {
        String message = "";
        Global.validationOk = false;
        initVariable();
        Data.init_val();
        nim = Data.getNbDataRetenue();
        //  System.out.println("nim= " +nim);
        if (nim < 12) {
            message
                    = "WARNING! few d.f. in the fit due to lags. Owing to " + Global.nb_classes_exploitees + " annual lags included in the model,\nonly " + nim + " years are used for ";
            message = message
                    + "fitting, which does not give enough degrees of freedom to the model.\nResults will be poor.\n";
            message = message
                    + "Despite this limitation I shall continue fitting.";
            MsgDialogBox msg = new MsgDialogBox(0, message, 1, Global.CadreMain);
        }

        f = Data.getF();
        v = Data.getV();
        vbar = Data.getVbar();
        pue = Data.getPue();
        res = new double[nim];
        fittedpue = new double[nim];
        variance_pue = Stat1.var(pue, false) * nim;
        //calc_var();
        calcul_val_init();
        for (int i = 0; i < nbre_par; i++) {
            par_init[i] = par_alors[i];
        }
          //  System.out.println( "Avant marquard");
        // for(int i=0;i<nbre_par;i++)System.out.print( par_alors[i]+" ");
        //  System.out.println();
        marquardt();
        sompar = somme_residus(par_alors);
        if (iter >= maxrit) {
            message
                    = "No convergence after" + maxrit + " iterations. I stop volontary the algorithm.";
            message = message
                    + " The fit will probably be poor and the validation might be impossible to run.";
            MsgDialogBox msg = new MsgDialogBox(0, message, 1, Global.CadreMain);

        }

        variance_pue = variance_pue / (double) nim;
        System.out.println(sompar+" et "+variance_pue);
        sompar = sompar / (double) nim;
        if (sompar > variance_pue) {
            sompar = variance_pue;
        }
        r = (1. - sompar / variance_pue);
        
        if (r < 0) {
            r = 0;
        }
        if (r == 0){
            message = "R2 is set to zero due to unexpected results in the variances (residual variance or total variance) computation";
            MsgDialogBox msg = new MsgDialogBox(0, message, 1, Global.CadreMain);
        }
            //System.out.println("Variance " + variance_pue);
        // System.out.println("Variance r�siduelle " + sompar);
        // System.out.println("R� " +r);
        aic = AICritere();
        Global.nbre_param = nbre_par;
        Global.val_param = new double[nbre_par];
        //  System.out.println( "Apr�s marquard");
        for (int i = 0; i < nbre_par; i++) {
            Global.val_param[i] = par_alors[i];
        }
        Global.coeff_determination = r;
        Global.critereAIC = aic;
           // for(int i=0;i<nim;i++)
        //     System.out.println("********"+res[i]);
        calc_residus();
        makePlot();
        // saveResult();
        Global.modelisationOk = true;

    }

    private static void calcul_val_init() {
        int l, k;
        double tmp;

        initcov();

        switch (Global.numero_modele) {
            case 0:
                nbre_par = 2;
                for (k = 0; k < nim; k++) {
                    ptmp[0] = 1;
                    ptmp[1] = f[k];
                    yy = pue[k];
                    itercov();
                }
                //inverse_gauss(nbre_par,covar,par_alors,covpy);
                inverse_gauss();
                break;
            case 1:
                nbre_par = 2;
                for (k = 0; k < nim; k++) {
                    ptmp[0] = 1;
                    ptmp[1] = f[k];
                    yy = Math.log(pue[k]);
                    itercov();
                }
                inverse_gauss();
                //inverse_gauss(nbre_par,covar,par_alors,covpy);
                par_alors[0] = Math.exp(par_alors[0]);
                break;
            case 2:
                nbre_par = 2;
                for (k = 0; k < nim; k++) {
                    ptmp[0] = 1;
                    ptmp[1] = vbar[k];
                    yy = pue[k];
                    itercov();
                }
                inverse_gauss();
                break;
            case 3:
                nbre_par = 2;
                for (k = 0; k < nim; k++) {
                    ptmp[0] = 1;
                    ptmp[1] = Math.log(vbar[k]);
                    yy = Math.log(pue[k]);
                    itercov();
                }
                inverse_gauss();
                par_alors[0] = Math.exp(par_alors[0]);
                break;
            case 4:
                nbre_par = 2;
                for (k = 0; k < nim; k++) {
                    ptmp[0] = 1;
                    ptmp[1] = vbar[k];
                    yy = pue[k];
                    itercov();
                }
                inverse_gauss();
                nbre_par = 3;
                par_alors[2] = 1.;
                break;
            case 5:
                nbre_par = 3;
                for (k = 0; k < nim; k++) {
                    ptmp[0] = vbar[k];
                    ptmp[1] = (vbar[k] * vbar[k]);
                    ptmp[2] = 1.;
                    yy = pue[k];
                    itercov();
                }
                inverse_gauss();
                break;
            case 6:
                nbre_par = 2;
                for (k = 0; k < nim; k++) {
                    ptmp[0] = 1;
                    ptmp[1] = f[k];
                    yy = pue[k];
                    itercov();
                }
                inverse_gauss();
                nbre_par = 3;
                par_alors[2] = 2.;
                break;
            case 7:
                nbre_par = 2;
                for (k = 0; k < nim; k++) {
                    ptmp[0] = vbar[k];
                    ptmp[1] = f[k];
                    yy = pue[k];
                    itercov();
                }
                inverse_gauss();
                par_alors[2] = par_alors[1];
                par_alors[1] = 1;
                nbre_par = 3;
                break;
            case 8:
                nbre_par = 3;
                for (k = 0; k < nim; k++) {
                    ptmp[0] = 1;
                    ptmp[1] = vbar[k];
                    ptmp[2] = f[k];
                    yy = pue[k];
                    itercov();
                }
                inverse_gauss();
                break;
            case 9:
                nbre_par = 2;
                for (k = 0; k < nim; k++) {
                    ptmp[0] = vbar[k];
                    ptmp[1] = f[k];
                    yy = pue[k];
                    itercov();
                }
                inverse_gauss();
                break;
            case 10:
                nbre_par = 4;
                for (k = 0; k < nim; k++) {
                    ptmp[0] = 1;
                    ptmp[1] = vbar[k];
                    ptmp[2] = (vbar[k] * vbar[k]);
                    ptmp[3] = f[k];
                    yy = pue[k];
                    itercov();
                }
                inverse_gauss();
                break;
            case 11:
                nbre_par = 3;
                for (k = 0; k < nim; k++) {
                    ptmp[0] = 1;
                    ptmp[1] = Math.log(vbar[k]);
                    ptmp[2] = f[k];
                    yy = Math.log(pue[k]);
                    itercov();
                }
                inverse_gauss();
                par_alors[0] = Math.exp(par_alors[0]);
                break;
            case 12:
                nbre_par = 2;
                for (k = 0; k < nim; k++) {
                    ptmp[0] = 1;
                    ptmp[1] = f[k];
                    yy = Math.log(pue[k]);
                    itercov();
                }
                inverse_gauss();
                par_alors[0] = Math.exp(par_alors[0]);
                par_alors[2] = par_alors[1];
                par_alors[1] = eps;
                nbre_par = 3;
                break;
            case 13:
                nbre_par = 2;
                for (k = 0; k < nim; k++) {
                    ptmp[0] = 1;
                    ptmp[1] = f[k];
                    yy = Math.log(pue[k] / vbar[k]);
                    itercov();
                }
                inverse_gauss();
                par_alors[0] = Math.exp(par_alors[0]);
                break;
            case 14:
                nbre_par = 2;
                for (k = 0; k < nim; k++) {
                    ptmp[0] = 1;
                    ptmp[1] = f[k];
                    yy = pue[k];
                    itercov();
                }
                inverse_gauss();
                par_alors[2] = -par_alors[1] / (par_alors[0] * par_alors[0]);
                par_alors[1] = eps;
                nbre_par = 3;
                break;
            case 15:
                nbre_par = 2;
                for (k = 0; k < nim; k++) {
                    ptmp[0] = 1;
                    ptmp[1] = f[k];
                    yy = pue[k];
                    itercov();
                }
                inverse_gauss();
                par_alors[2] = par_alors[1];
                par_alors[1] = eps;
                nbre_par = 3;
                break;
            case 16:
                nbre_par = 2;
                for (k = 0; k < nim; k++) {
                    ptmp[0] = v[k];
                    ptmp[1] = v[k] * vbar[k] * f[k];
                    yy = pue[k];
                    itercov();
                }
                inverse_gauss();
                break;
            case 17:
                nbre_par = 2;
                for (k = 0; k < nim; k++) {
                    ptmp[0] = 1;
                    ptmp[1] = f[k];
                    yy = Math.log(pue[k]);
                    itercov();
                }
                inverse_gauss();
                par_alors[0] = Math.exp(par_alors[1]);
                par_alors[2] = par_alors[1];
                par_alors[1] = eps;
                nbre_par = 3;
                break;
            case 18:
                nbre_par = 2;
                for (k = 0; k < nim; k++) {
                    ptmp[0] = 1;
                    ptmp[1] = f[k];
                    yy = Math.log(pue[k]);
                    itercov();
                }
                inverse_gauss();
                par_alors[0] = Math.exp(par_alors[0]);
                par_alors[2] = -par_alors[1] / par_alors[0];
                par_alors[1] = eps;
                nbre_par = 3;
                break;
            case 19:
                nbre_par = 2;
                for (k = 0; k < nim; k++) {
                    ptmp[0] = 1;
                    ptmp[1] = f[k] * vbar[k];
                    yy = Math.log(pue[k] / v[k]);
                    itercov();
                }
                inverse_gauss();
                par_alors[0] = Math.exp(par_alors[0]);
                break;
            case 20:
                nbre_par = 2;
                for (k = 0; k < nim; k++) {
                    ptmp[0] = 1;
                    ptmp[1] = f[k];
                    yy = Math.log(pue[k]);
                    itercov();
                }
                inverse_gauss();
                nbre_par = 4;
                par_alors[0] = Math.exp(par_alors[0]);
                par_alors[2] = eps;
                par_alors[3] = eps;
                break;
            case 21:
                nbre_par = 2;
                for (k = 0; k < nim; k++) {
                    ptmp[0] = 1;
                    ptmp[1] = f[k];
                    yy = Math.log(pue[k] / vbar[k]);
                    itercov();
                }
                inverse_gauss();
                par_alors[0] = Math.exp(par_alors[0]);
                par_alors[2] = par_alors[1];
                par_alors[1] = eps;
                nbre_par = 3;
                break;
            case 22:
                nbre_par = 2;
                for (k = 0; k < nim; k++) {
                    ptmp[0] = vbar[k];
                    ptmp[1] = f[k];
                    yy = pue[k];
                    itercov();
                }
                inverse_gauss();
                par_alors[2] = par_alors[1];
                par_alors[1] = 1;
                par_alors[3] = 2;
                nbre_par = 4;
                break;
            case 23:
                nbre_par = 3;
                for (k = 0; k < nim; k++) {
                    ptmp[0] = vbar[k];
                    ptmp[1] = (vbar[k] * vbar[k]);
                    ptmp[2] = f[k];
                    yy = pue[k];
                    itercov();
                }
                inverse_gauss();
                par_alors[3] = 2;
                nbre_par = 4;
                break;
            case 24:
                nbre_par = 2;
                for (k = 0; k < nim; k++) {
                    ptmp[0] = v[k];
                    ptmp[1] = v[k] * vbar[k] * f[k];
                    yy = pue[k];
                    itercov();
                }
                inverse_gauss();
                par_alors[3] = -par_alors[1];
                par_alors[1] = 1.0;
                par_alors[2] = eps;
                nbre_par = 4;
                break;
            case 25:
                nbre_par = 2;
                for (k = 0; k < nim; k++) {
                    ptmp[0] = 1;
                    ptmp[1] = f[k] * vbar[k];
                    yy = Math.log(pue[k] / v[k]);
                    itercov();
                }
                inverse_gauss();
                par_alors[0] = Math.exp(par_alors[0]);
                par_alors[2] = par_alors[1];
                par_alors[1] = eps;
                nbre_par = 3;
                break;
            case 26:
                nbre_par = 2;
                for (k = 0; k < nim; k++) {
                    ptmp[0] = v[k];
                    ptmp[1] = f[k] * (v[k] * v[k]);
                    yy = pue[k];
                    itercov();
                }
                inverse_gauss();
                par_alors[0] = par_alors[0];
                par_alors[3] = par_alors[1];
                par_alors[1] = 1.0;
                par_alors[2] = eps;
                nbre_par = 4;
                break;
            case 27:
                nbre_par = 3;
                for (k = 0; k < nim; k++) {
                    ptmp[0] = v[k];
                    ptmp[1] = v[k] * vbar[k];
                    ptmp[2] = f[k];
                    yy = pue[k];
                    itercov();
                }
                inverse_gauss();
                par_alors[0] = par_alors[0];
                par_alors[3] = par_alors[2];
                par_alors[2] = par_alors[1];
                par_alors[1] = eps;
                nbre_par = 4;
                break;
            case 28:
                nbre_par = 3;
                for (k = 0; k < nim; k++) {
                    ptmp[0] = 1;
                    ptmp[1] = Math.log(v[k]);
                    ptmp[2] = f[k];
                    yy = Math.log(pue[k]);
                    itercov();
                }
                inverse_gauss();
                par_alors[0] = Math.exp(par_alors[1]);
                par_alors[3] = eps;
                nbre_par = 4;
                break;
            case 29:
                nbre_par = 3;
                for (k = 0; k < nim; k++) {
                    ptmp[0] = 1;
                    ptmp[1] = Math.log(vbar[k]);
                    ptmp[2] = f[k];
                    yy = Math.log(pue[k]);
                    itercov();
                }
                inverse_gauss();
                par_alors[0] = Math.exp(par_alors[0]);
                par_alors[3] = eps;
                nbre_par = 4;
                break;
            case 30:
                nbre_par = 2;
                for (k = 0; k < nim; k++) {
                    ptmp[0] = v[k];
                    ptmp[1] = v[k] * vbar[k];
                    yy = pue[k];
                    itercov();
                }
                inverse_gauss();
                par_alors[0] = par_alors[0];
                par_alors[2] = par_alors[1];
                par_alors[1] = eps;
                par_alors[3] = eps;
                nbre_par = 4;
                break;
        }

    }

    private static void initcov() {
        int i, j;
        for (i = 0; i < nparmax; i++) {
            for (j = 0; j < nparmax; j++) {
                covar[i][j] = 0;
            }
        }
        for (j = 0; j < nparmax; j++) {
            covpy[j] = 0;
        }
        for (j = 0; j < nparmax; j++) {
            ptmp[j] = 0;
        }
    }

    private static void itercov() {
        int i, j;
        for (i = 0; i < nbre_par; i++) {
            for (j = 0; j < nbre_par; j++) {
                covar[i][j] += ptmp[i] * ptmp[j];
            }
        }
        for (j = 0; j < nbre_par; j++) {
            covpy[j] += ptmp[j] * yy;
        }

    }

    private static void inverse_gauss() {
        int i, j, k;
        double c, s;
        for (i = 0; i < nbre_par - 1; i++) {
            for (j = i + 1; j < nbre_par; j++) {
                c = covar[j][i] / covar[i][i];
                for (k = i + 1; k < nbre_par; k++) {
                    covar[j][k] = covar[j][k] - c * covar[i][k];
                }
                covpy[j] = covpy[j] - c * covpy[i];
            }
        }
        par_alors[nbre_par - 1] = covpy[nbre_par - 1] / covar[nbre_par - 1][nbre_par - 1];
        for (i = nbre_par - 2; i >= 0; i--) {
            s = covpy[i];
            for (k = i + 1; k < nbre_par; k++) {
                s -= covar[i][k] * par_alors[k];
            }
            par_alors[i] = s / covar[i][i];
        }
    }
    /*
     private static void inverse_gauss(int nn,double[][]aa,double[]xx,double[]yy)
     {
     int 	i,j,k;
     double 	c,s;
     for(i=0;i<nn-1;i++) {
     for(j=i+1;j<nn;j++){
     c=aa[j][i]/aa[i][i];
     for(k=i+1;k<nn;k++) aa[j][k]=aa[j][k]-c*aa[i][k];
     yy[j]=yy[j]-c*yy[i];
     }
     }
     xx[nn-1] = yy[nn-1]/aa[nn-1][nn]-1;
     for(i=nn-2;i>=0;i--){
     s=yy[i];
     for(k=i+1;k<nn;k++) s -= aa[i][k]*xx[k];
     xx[i]= s/aa[i][i];
     }
     }
     */

    private static void marquardt() {
        int i, j, k;
        double c, s;
        for (i = 0; i < nbre_par; i++) {
            par_alors[i] = par_init[i];
        }
        iter = 0;
        ritertot = 0;
        val_fin = somme_residus(par_alors);
        //System.out.println("valfin  "+val_fin);
        resiter[0] = val_fin;
        ecart = 0.1;
        riter = 0;
        while ((iter < maxrit - 1)
                && ((ecart > 0.05) || (iter < 40))
                && (ecart > 0.0001)
                && (riter > (-17))) {
            iter++;
            /* System.out.println( "Pendant marquard");
             for(int b=0;b<nbre_par;b++) {
             System.out.println( "Iteration " +iter);
             System.out.println(b + " "+ par_alors[b]);
             }*/
            val_init = val_fin;
            calcul_derivee_hess();
            for (i = 0; i < nbre_par; i++) {
                racineh[i] = Math.sqrt(hessien[i][i]) + 1.E-15;
            }
            for (i = 0; i < nbre_par; i++) {
                derivee[i] /= racineh[i];
            }
            for (i = 0; i < nbre_par; i++) {
                for (j = 0; j < nbre_par; j++) {
                    hessien[i][j] /= (racineh[i] * racineh[j]);
                }
            }
            for (i = 0; i < nbre_par; i++) {
                hessien[i][i] += lambda;
            }
            /*for(i=0;i<nbre_par;i++)
             System.out.println("racnan "+racineh[i]+" deri " +derivee[i] );*/
                //*******************************************
            //inverse_gauss( nbre_par, hessien, delta, derivee );
            for (i = 0; i < nbre_par - 1; i++) {
                for (j = i + 1; j < nbre_par; j++) {
                    c = hessien[j][i] / hessien[i][i];
                    for (k = i + 1; k < nbre_par; k++) {
                        hessien[j][k] = hessien[j][k] - c * hessien[i][k];
                    }
                    derivee[j] = derivee[j] - c * derivee[i];
                }
            }
            delta[nbre_par - 1] = derivee[nbre_par - 1] / hessien[nbre_par - 1][nbre_par - 1];//bug ici
            for (i = nbre_par - 2; i >= 0; i--) {
                s = derivee[i];
                for (k = i + 1; k < nbre_par; k++) {
                    s -= hessien[i][k] * delta[k];
                }
                delta[i] = s / hessien[i][i];
            }
            //
            for (i = 0; i < nbre_par; i++) {
                //System.out.println("delta "+ delta[i]);
                delta[i] /= racineh[i];
            }
            for (i = 0; i < nbre_par; i++) {
                par_inter[i] = par_alors[i] - delta[i];
            }
            val_fin = somme_residus(par_inter);
            //System.out.println("valfin "+ iter+" " +val_fin);
            riter = 0;
            if (val_fin < val_init) {
                while (val_fin < val_init) {
                    val_init = val_fin;
                    for (i = 0; i < nbre_par; i++) {
                        delta[i] *= alpha;
                        par_inter[i] = par_alors[i] - delta[i];
                    }
                    val_fin = somme_residus(par_inter);
                    riter++;
                    ritertot++;
                }
                for (i = 0; i < nbre_par; i++) {
                    delta[i] /= alpha;
                    par_inter[i] = par_alors[i] - delta[i];
                }
                val_fin = somme_residus(par_inter);
            } else {
                while (val_fin > val_init) {
                    for (i = 0; i < nbre_par; i++) {
                        delta[i] /= alpha;
                        par_inter[i] = par_alors[i] - delta[i];
                    }
                    val_fin = somme_residus(par_inter);
                    riter--;
                    if (riter < (-15)) {
                        for (i = 0; i < nbre_par; i++) {
                            delta[i] = 0;
                        }
                    }
                    ritertot++;
                }
            }
            val_fin = somme_residus(par_inter);
            if (riter < 0) {
                lambda *= rl;
            } else {
                lambda /= rl;
            }
            resiter[iter] = val_fin;
            if (iter > 5) {
                ecart = 100 * (resiter[iter - 5] - resiter[iter]) / variance_pue;
            }
            //impression_iter();
            boolean bNan = false;
            while (bNan == false && i < nbre_par) {
                for (i = 0; i < nbre_par; i++) {
                    bNan = Double.isNaN(par_inter[i]);
                }
            }

            if (!bNan) {
                for (i = 0; i < nbre_par; i++) {
                    par_alors[i] = par_inter[i];
                }
            }
        }
    }

    private static double somme_residus(double[] par) {
        double cc = 0;
        int i;
        for (i = 0; i < nim; i++) {
            double c = EquationModele.fonction_modele(f[i], v[i], vbar[i], par) - pue[i];
            cc += (c * c);
        }
        return (cc);
    }

    private static void calcul_derivee_hess() {
        int i, j, n;
        double v1, v2;
        for (i = 0; i < nbre_par; i++) {
            par[i] = par_alors[i];
            dpar[i] = mul * par[i];
        }
        for (n = 0; n < nim; n++) {
            fittedpue[n] = EquationModele.fonction_modele(f[n], v[n], vbar[n], par);
               // v1 = EquationModele.fonction_modele(f[n],v[n],vbar[n],par);
            //System.out.println("v1 " +n+ "  "+ v1);
            res[n] = fittedpue[n] - pue[n];
            for (i = 0; i < nbre_par; i++) {
                par[i] += dpar[i];
                v2 = EquationModele.fonction_modele(f[n], v[n], vbar[n], par);
                //System.out.println("v2 " +i+ "  "+ v2);
                derfonc[n][i] = (v2 - fittedpue[n]) / dpar[i];
                //derfonc[n][i] = (v2 - v1)/dpar[i];
                par[i] -= dpar[i];
            }
        }
        for (i = 0; i < nbre_par; i++) {
            derivee[i] = 0;
            for (n = 0; n < nim; n++) {
                derivee[i] += (res[n] * derfonc[n][i]);
            }
            //System.out.println("derivenan " +i+ "  "+ derivee[i]);

        }
        for (i = 0; i < nbre_par; i++) {
            for (j = 0; j <= i; j++) {
                hessien[i][j] = 0;
                for (n = 0; n < nim; n++) {
                    hessien[i][j] += (derfonc[n][i] * derfonc[n][j]);
                }
                hessien[j][i] = hessien[i][j];
                // System.out.println("hessien " +i+ "  "+ hessien[i][j]);
            }
        }

    }

    private static void calc_var() {
        int i;
        double s1 = 0, s2 = 0;
        for (i = 0; i < nim; i++) {
            s1 += pue[i];
            s2 += (pue[i] * pue[i]);
        }
        variance_pue = s2 - (s1 * s1) / (double) nim;

 //System.out.println(Stat1.var(pue,false));
    }

    private static void calc_residus() {
        for (int i = 0; i < nim; i++) {
            fittedpue[i] = EquationModele.fonction_modele(f[i], v[i], vbar[i], par);
            res[i] = pue[i] - fittedpue[i];
            // System.out.println("------"+res[i]);
        }
    }

    private static double AICritere() {

        double sse = somme_residus(par_alors);
        return nim * Math.log((sse / (nim + 1.E-15)) + 1.E-15) + 2 * nbre_par + nim;

    }

    private static void makePlot() {
        /**
         * ***************** cpue plot *****************************
         */
        double minx = Stat1.min(Data.getYears());
        PlotSerie[] ps = new PlotSerie[5];
        ps[0] = new PlotSerie("x", Data.getYears(), "Observed Pue", Data.getPue());
        ps[1] = new PlotSerie("x", Data.getYears(), "Fitted Pue", fittedpue);
        ps[2] = new PlotSerie("x", Data.getYears(), "Residuals", res);

        Global.fittedCpuePlot[0] = new Plot();
        ps[0].setFigure(2);
        ps[1].setFigure(2);
        ps[0].setCouleur(Color.blue);
        Global.fittedCpuePlot[0].setValeurs(ps[0]);
        Global.fittedCpuePlot[0].setValeurs(ps[1]);
        Global.fittedCpuePlot[0].ShowLegend = true;

        Global.fittedCpuePlot[1] = new Plot();
        ps[2].setFigure(2);
        Global.fittedCpuePlot[1].setValeurs(ps[2]);
        Global.fittedCpuePlot[1].ajusteExtremas(false, true, false, false);
        Global.fittedCpuePlot[1].setXcutYat(0);

        for (int i = 0; i < 2; i++) {
            Global.fittedCpuePlot[i].setTitreGraphique(Global.titreG[13 + i]);
            Global.fittedCpuePlot[i].setTitreX(Global.titreSx[13 + i]);
            Global.fittedCpuePlot[i].setTitreY(Global.titreSy[13 + i]);
            Global.fittedCpuePlot[i].setDecimalsurX(0);
            Global.fittedCpuePlot[i].setMinAxeX(minx - 1);
            Global.fittedCpuePlot[i].setpasX(2.0d);
        }
        /**
         * ****************** residuals plot **************************
         */

        for (int i = 0; i < 2; i++) {
            Global.residualsPlot[i] = new Plot();
        }

        Global.residualsPlot[0].setValeurs(new PlotSerie("", Data.getF(), "", res));
        Global.residualsPlot[1].setValeurs(new PlotSerie("", Data.getV(), "", res));
        for (int i = 0; i < 2; i++) {
            Global.residualsPlot[i].setTitreGraphique(Global.titreG[i + 15]);
            Global.residualsPlot[i].setTitreX(Global.titreSx[i + 15]);
            Global.residualsPlot[i].setTitreY(Global.titreSy[i + 15]);
            Global.residualsPlot[i].ajusteExtremas(false, true, false, false);
            Global.residualsPlot[i].setXcutYat(0.0d);
        }

        /**
         * *********************variate plot ******************************
         */
        double pasx, pasy, minix, miniy, maxix, maxiy, xx, yy;
        double[] estim1 = new double[nim];
        double[] estim2 = new double[nim];
        double[] tabxx = new double[nim];
        DecimalFormat df = new DecimalFormat("0.0");
        if ((Global.numero_modele > 5) || (Global.numero_modele < 2)) {
            double[] ext = Stat1.Extremas(Data.getF());
            minix = ext[0];
            if (minix > 0) {
                minix = 0;
            }
            maxix = ext[1];
            pasx = (maxix - minix) / nim;

            ext = Stat1.Extremas(Data.getV());
            miniy = ext[0];
            maxiy = ext[1];
            pasy = (maxiy - miniy) / 4.0;
            xx = minix;
//xx=minix+pasx;
            yy = miniy;
            PlotSerie[] pvs = new PlotSerie[8];
            Global.variatePlot[0] = new Plot();
            Global.variatePlot[1] = new Plot();
            pvs[0] = new PlotSerie("Effort (E)", Data.getF(), "CPUE", Data.getPue());
            pvs[1] = new PlotSerie("Effort (E)", Data.getF(), "Production (Y)", Data.getYexp());
            pvs[0].setCouleur(Color.black);
            pvs[1].setCouleur(Color.black);
            Global.variatePlot[0].setValeurs(pvs[0]);
            Global.variatePlot[0].setTitreGraphique("Function CPUE versus V&E");
            Global.variatePlot[1].setValeurs(pvs[1]);
            Global.variatePlot[1].setTitreGraphique("Function Y versus V&E");
            pvs[0].setFigure(2);
            pvs[1].setFigure(2);
            pvs[0].setMark(2);
            pvs[1].setMark(3);
            for (int j = 0; j < 5; j++) {
                for (int i = 0; i < nim; i++) {
                    estim1[i] = EquationModele.fonction_modele(xx, yy, yy, par_alors);
                    estim2[i] = xx * estim1[i];
                    tabxx[i] = xx;
                    xx = xx + pasx;
                }
                if (j == 0) {
                    pvs[2] = new PlotSerie("Effort (E)", tabxx, "Vmini:" + df.format(yy).toString(), estim1);
                    pvs[3] = new PlotSerie("Effort (E)", tabxx, "Vmini:" + df.format(yy).toString(), estim2);
                    pvs[2].setFigure(2);
                    pvs[3].setFigure(2);
                    pvs[2].setCouleur(Color.blue);
                    pvs[3].setCouleur(Color.blue);
                    Global.variatePlot[0].setValeurs(pvs[2]);
                    Global.variatePlot[1].setValeurs(pvs[3]);
                } else if (j == 4) {
                    pvs[4] = new PlotSerie("Effort (E)", tabxx, "Vmaxi:" + df.format(yy).toString(), estim1);
                    pvs[5] = new PlotSerie("Effort (E)", tabxx, "Vmaxi:" + df.format(yy).toString(), estim2);
                    Global.variatePlot[0].setValeurs(pvs[4]);
                    Global.variatePlot[1].setValeurs(pvs[5]);
                    pvs[4].setFigure(2);
                    pvs[5].setFigure(2);

                } else if (j == 2 && ((Global.numero_modele == 10) || (Global.numero_modele == 21) || (Global.numero_modele == 23) || (Global.numero_modele == 24) || (Global.numero_modele == 25) || (Global.numero_modele == 27) || (Global.numero_modele == 30))) {
                    pvs[6] = new PlotSerie("Effort (E)", tabxx, "Vmiddle:" + df.format(yy).toString(), estim1);
                    pvs[7] = new PlotSerie("Effort (E)", tabxx, "Vmiddle:" + df.format(yy).toString(), estim2);
                    pvs[6].setCouleur(Color.green);
                    pvs[7].setCouleur(Color.green);
                    pvs[6].setFigure(2);
                    pvs[7].setFigure(2);
                    Global.variatePlot[0].setValeurs(pvs[6]);
                    Global.variatePlot[1].setValeurs(pvs[7]);
                }
                xx = minix;//+pasx;
                yy = yy + pasy;

            }

            Global.variatePlot[0].setMinAxeX(0);
            Global.variatePlot[1].setMinAxeX(0);
            Global.variatePlot[0].setMinAxeY(0);
            Global.variatePlot[1].setMinAxeY(0);
            Global.variatePlot[0].ShowLegend = true;
            Global.variatePlot[1].ShowLegend = true;
        } else //traitement des mod�les � Environement pr�pond�rant
        {
            double[] ext = Stat1.Extremas(Data.getV());
            minix = ext[0];
            if (minix > 0) {
                minix = 0;
            }
            maxix = ext[1];
            pasx = (maxix - minix) / nim;
            ext = Stat1.Extremas(Data.getPue());
            miniy = ext[0];
            maxiy = ext[1];
            PlotSerie[] pvs = new PlotSerie[8];
            Global.variatePlot[0] = new Plot();
            Global.variatePlot[1] = null;//new Plot();
            pvs[0] = new PlotSerie("Environment (V)", Data.getV(), "CPUE", Data.getPue());
            pvs[0].setCouleur(Color.black);
            pvs[0].setFigure(2);
            pvs[0].setMark(2);
            Global.variatePlot[0].setValeurs(pvs[0]);
            Global.variatePlot[0].setTitreGraphique("Function CPUE versus V");

        }
    }

    /*
     Donne les principaux r�sultats
     */
    public static Object[][] getResult() {
        if (!Global.modelisationOk) {
            return null;
        }
        String[] param$ = {"     a    ", "     b    ", "     c    ", "     d    "};
        String[] title$ = {"Parameters  ", "Actual value ", "Initial value"};
        Object[][] data$ = new String[nbre_par + 9][3];
        DecimalFormat nf = new DecimalFormat(" 0.000000;-0.000000");
        DecimalFormat nf2 = new DecimalFormat(" 0.00;-0.00");
        for (int j = 0; j < 3; j++) {
            data$[0][j] = title$[j];
        }

        for (int i = 0; i < nbre_par; i++) {
            data$[i + 1][0] = param$[i];
            data$[i + 1][1] = nf.format(par_alors[i]);
            data$[i + 1][2] = nf.format(par_init[i]);
        }
        data$[nbre_par + 1][0] = "Nb. of data used to fit";
        data$[nbre_par + 1][1] = Integer.toString(nim);
        data$[nbre_par + 2][0] = "Nb. of degrees of freedom";
        data$[nbre_par + 2][1] = Integer.toString(nim - nbre_par);
        data$[nbre_par + 3][0] = "Coefficient of determination R²";
        data$[nbre_par + 3][1] = nf2.format(r);
        
        System.out.println("r : "+r+" et globalr : "+Global.coeff_determination);
        
        data$[nbre_par + 4][0] = "Fisher test of R² for F(" + getNbDataRetenue() + "," + Global.nbre_param +")";
        data$[nbre_par + 4][1] = Double.toString(Global.pfisher(Global.coeff_determination, getNbDataRetenue(), Global.nbre_param));
        
        data$[nbre_par + 5][0] = "Fisher threshold";
        data$[nbre_par + 5][1] = nf.format(Global.fF(Global.coeff_determination, getNbDataRetenue(), Global.nbre_param));        
        
        data$[nbre_par + 6][0] = "Akaike's Information Criterion ";
        data$[nbre_par + 6][1] = nf.format(aic);
        data$[nbre_par + 7][0] = "Variance                       ";
        data$[nbre_par + 7][1] = nf.format(variance_pue);
        data$[nbre_par + 8][0] = "Residual Variance              ";
        data$[nbre_par + 8][1] = nf.format(sompar);

        return data$;

    }

    /*
     Donne les  r�sultats d�taill�s
     */
    public static Object[][] getYearResult() {
        if (!Global.modelisationOk) {
            return null;
        }
        String[] title$ = {"Years", "Observed Cpue", "Fitted Cpue", "Residuals"};
        Object[][] data$ = new String[nim + 1][4];
        DecimalFormat nf = new DecimalFormat(" 0.000000;-0.000000");
        DecimalFormat nf0 = new DecimalFormat("0");
        double[] p = Data.getPue();
        double[] y = Data.getYears();
        for (int j = 0; j < 4; j++) {
            data$[0][j] = title$[j];
        }

        for (int i = 0; i < nim; i++) {
            data$[i + 1][0] = nf0.format(y[i]);
            data$[i + 1][1] = nf.format(p[i]);
            data$[i + 1][2] = nf.format(fittedpue[i]);
            data$[i + 1][3] = nf.format(res[i]);
        }

        return data$;

    }

    private static void initVariable() {

        nim = 0;
        nbre_par = 0;
        lambda = 16;
        riter = 0;
        iter = 0;
        ritertot = 0;
        val_init = 0;
        val_fin = 0;
        ecart = 0;
        r = 0;
        sompar = 0;
        variance_pue = 0;
        yy = 0;

        ptmp = new double[nparmax + 2];
        covar = new double[nparmax + 1][nparmax + 1];

        hessien = new double[nparmax + 1][nparmax + 1];
        covpy = new double[nparmax + 2];
        par_alors = new double[nparmax + 2];
        racineh = new double[nparmax + 2];
        derivee = new double[nparmax + 2];
        delta = new double[nparmax + 2];
        par_init = new double[nparmax + 2];
        par_inter = new double[nparmax + 2];
        spar = new double[nparmax + 2];
        par = new double[nparmax + 2];
        dpar = new double[nparmax + 2];
        sompari = new double[nparmax + 2];
        //f,v,vbar,pue,res,fittedpue;
        resiter = new double[maxrit];
        derfonc = new double[61][nparmax + 1];

    }

    /*private static void saveResult(){
     String [] param$={"a","b","c","d"};
     try
     {
     PrintWriter out=new PrintWriter(new FileOutputStream("stat.txt",true));
     out.println(RechercheModele.getEquation());
     out.println(nim + " exploitée " + Global.nb_classes_exploitees+" recrutement  " +Global.recruitment_age + " debut "+ Global.begin_influence_period + " fin " +Global.end_influence_period);
     for(int i=0;i<nbre_par;i++)
     out.println(param$[i] +"  " +par_alors[i] + "  " + par_init[i]);
     out.println("r� " +   r);
     out.println("");
     out.close();
     }
     catch(IOException ieo)
     {}

     }
     */
}
