/**
 * Titre : Climprod<p>
 *
 */
package fr.ird.climprod;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import java.awt.event.*;
import java.io.InputStream;
import javax.swing.table.TableColumn;
import java.util.Vector;

public class Cadre_ModeleDirect extends JFrame {

    JPanel jPanChoix = new JPanel();
    JPanel jPanCmdChoix = new JPanel();
    JPanel jPanBiologie = new JPanel();
    JPanel jPanResults = new JPanel();

    JRadioButton chkE = new JRadioButton();
    JRadioButton chkV = new JRadioButton();
    JRadioButton chkEVCatchability = new JRadioButton();
    JRadioButton chkEVAbundance = new JRadioButton();
    JRadioButton chkEVBoth = new JRadioButton();
    ButtonGroup group = new ButtonGroup();
  //JButton cmdE = new JButton();
    //JButton cmdV = new JButton();
    // JButton cmdEVCatchability = new JButton();
    //JButton cmdEVAbundance = new JButton();
    // JButton cmdEVBoth = new JButton();
    JList<String> jlstChoix = new JList<String>();

    JScrollPane jScrollPane1 = new JScrollPane();
    JScrollPane jScrollPane2 = new JScrollPane();
    JScrollPane jScrollPane3 = new JScrollPane();
    JScrollPane jScrollPane4 = new JScrollPane();

    GridBagLayout gridBagLayout1 = new GridBagLayout();
    GridBagLayout gridBagLayout2 = new GridBagLayout();

    JLabel jlblExploitedYears = new JLabel();
    JLabel jlblRecrutement = new JLabel();
    JLabel jlblBegining = new JLabel();
    JLabel jlblEnd = new JLabel();
    JLabel lblModele = new JLabel();
    JComboBox<String> jcboExploitedYears = new JComboBox<String>();
    JComboBox<String> jcboRecrutement = new JComboBox<String>();
    JComboBox<String> jcboBegining = new JComboBox<String>();
    JComboBox<String> jcboEnd = new JComboBox<String>();

    JTable jTableModelisation = new JTable();
    JTable jTableValidation = new JTable();
    DefaultListModel<String> modele = new DefaultListModel<String>();
    JTextArea jTextAreaInfoModel = new JTextArea();

    JToolBar jToolBar1 = new JToolBar();
    JButton cmdFit = new JButton();
    JButton cmdFitted = new JButton();
    JButton cmdTVariate = new JButton();
    JButton cmdJackniffe = new JButton();
    JButton cmdEnd = new JButton();
    JButton cmdPredict = new JButton();

  //JPopupMenu popup=new JPopupMenu();
    // JMenuItem menuPlotFitted = new JMenuItem("Observed-Fitted and Residuals CPUE plots");
    //JMenuItem menuPlotJacknife = new JMenuItem("Jacknife Plots");
    // JMenuItem menuPlotThreeVariate = new JMenuItem("Three-variate plots");
    int nbFit;
    Vector<Cadre_SplitPlot> vPlot = new Vector<Cadre_SplitPlot>();

    public Cadre_ModeleDirect() {
        try {
            nbFit = 0;
            initWindow();
            UtilCadre.Size(this, 80, 85);
            UtilCadre.Centrer(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initWindow() throws Exception {
        this.setIconImage(Toolkit.getDefaultToolkit().createImage(Cadre_ModeleDirect.class.getResource("resources/images/Climprod.jpg")));

        this.setTitle("Climprod: Fit a model directly");
        this.addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                this_windowClosing(e);
            }
        });
        this.getContentPane().setLayout(gridBagLayout2);
        jPanChoix.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(142, 142, 142)), "Select a model"));
        jPanChoix.setLayout(gridBagLayout1);
        jPanCmdChoix.setLayout(new GridLayout(5, 2));
        chkE.setText("Simple models CPUE=f(E)");
        chkE.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                chkE_actionPerformed(e);
            }
        });

        chkV.setText("Simple models CPUE=f(V)");
        chkV.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                chkV_actionPerformed(e);
            }
        });
        chkEVCatchability.setText("Mixed models CPUE=f(E,V), where V influence Catchability");
        chkEVCatchability.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                chkEVCatchability_actionPerformed(e);
            }
        });
        chkEVAbundance.setText("Mixed models CPUE=f(E,V), where V influence Abundance");
        chkEVAbundance.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                chkEVAbundance_actionPerformed(e);
            }
        });
        chkEVBoth.setText("Mixed models CPUE=f(E,V), where V influence Abundance & Catchability");
        chkEVBoth.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                chkEVBoth_actionPerformed(e);
            }
        });

        jScrollPane2.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(142, 142, 142)), "Selected model informations"));
        jPanBiologie.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(142, 142, 142)), "Stock characteristics"));
        jPanBiologie.setLayout(new GridLayout(4, 2));
        jlblExploitedYears.setText("Number of significantly exploited years classes");
        jlblRecrutement.setText("Age at recruitment");
        jlblBegining.setText("Age at the beginning of environmental influence");
        jlblEnd.setText("Age at the end of environmental influence");
        cmdFit.setText("Fit the selected model");

        cmdFit.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                cmdFit_actionPerformed(e);
            }
        });
        lblModele.setText("Model fitting:");
        jScrollPane3.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(142, 142, 142)), "Modelization: Main results"));
        jTextAreaInfoModel.setLineWrap(true);
        jTextAreaInfoModel.setWrapStyleWord(true);
        jlstChoix.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                jlstChoix_valueChanged(e);
            }
        });
        this.addWindowListener(new java.awt.event.WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent e) {
                Global.CadreModeleDirect = null;
            }
        });
        jPanResults.setLayout(new GridLayout(1, 2, 5, 0));
    //gridLayout3.setColumns(2);
        //gridLayout3.setHgap(5);

        jScrollPane4.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(142, 142, 142)), "Validation:Main results"));
        this.getContentPane().add(jPanChoix, new GridBagConstraints(1, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(5, 0, 0, 5), 0, 0));

        jPanChoix.add(jPanCmdChoix, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 0, 5), 0, 0));
        group.add(chkE);
        group.add(chkV);
        group.add(chkEVCatchability);
        group.add(chkEVAbundance);
        group.add(chkEVBoth);
        jPanCmdChoix.add(chkE, null);
        jPanCmdChoix.add(chkV, null);
        jPanCmdChoix.add(chkEVCatchability, null);
        jPanCmdChoix.add(chkEVAbundance, null);
        jPanCmdChoix.add(chkEVBoth, null);
        jPanChoix.add(jScrollPane1, new GridBagConstraints(1, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 100, 0));
        jScrollPane1.getViewport().add(jlstChoix, null);
        jPanChoix.add(jScrollPane2, new GridBagConstraints(0, 1, 2, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 2, 2), 0, 67));
        jScrollPane2.getViewport().add(jTextAreaInfoModel, null);
        this.getContentPane().add(jPanBiologie, new GridBagConstraints(0, 1, 2, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 0, 5), 0, 0));

        String[] item1 = QuestionReponse.getItemQuestion("nb_classes_exploitees");
        String[] item2 = QuestionReponse.getItemQuestion("begin_influence_period");
        for (String item : item1) {
            jcboRecrutement.addItem(item);
            jcboExploitedYears.addItem(item);
        }
        for (String item : item2) {
            jcboBegining.addItem(item);
            jcboEnd.addItem(item);
        }

        if (Global.nb_classes_exploitees < 1) {
            jcboExploitedYears.setSelectedIndex(0);
        } else {
            jcboExploitedYears.setSelectedIndex(Global.nb_classes_exploitees - 1);
        }
        if (Global.recruitment_age < 1) {
            jcboRecrutement.setSelectedIndex(0);
        } else {
            jcboRecrutement.setSelectedIndex(Global.recruitment_age - 1);
        }

        if (Global.begin_influence_period < 1) {
            jcboBegining.setSelectedIndex(0);
        } else {
            jcboBegining.setSelectedIndex(Global.begin_influence_period);
        }
        if (Global.end_influence_period < 1) {
            jcboEnd.setSelectedIndex(0);
        } else {
            jcboEnd.setSelectedIndex(Global.end_influence_period);
        }

        jPanBiologie.add(jlblExploitedYears, null);
        jPanBiologie.add(jcboExploitedYears, null);
        jPanBiologie.add(jlblRecrutement, null);
        jPanBiologie.add(jcboRecrutement, null);
        jPanBiologie.add(jlblBegining, null);
        jPanBiologie.add(jcboBegining, null);
        jPanBiologie.add(jlblEnd, null);
        jPanBiologie.add(jcboEnd, null);
        jPanResults.add(jScrollPane3, null);
        jPanResults.add(jScrollPane4, null);
        jScrollPane4.getViewport().add(jTableValidation, null);
        jScrollPane3.getViewport().add(jTableModelisation, null);
        this.getContentPane().add(cmdFit, new GridBagConstraints(0, 2, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(2, 5, 0, 5), 0, 0));
        this.getContentPane().add(lblModele, new GridBagConstraints(0, 3, 2, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(10, 10, 0, 8), 0, 0));

        this.getContentPane().add(jPanResults, new GridBagConstraints(0, 4, 2, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 5, 5, 5), 0, 100));
        this.getContentPane().add(jToolBar1, new GridBagConstraints(0, 5, 2, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 5, 5, 5), 0, 0));

 // popup.add(menuPlotFitted);
        // popup.addSeparator();
        /// popup.add(menuPlotThreeVariate);
        /// popup.add(menuPlotJacknife);
        cmdFitted.setIcon(new ImageIcon(Cadre_ModeleDirect.class.getResource("resources/images/FittedPlot.gif")));
        cmdJackniffe.setIcon(new ImageIcon(Cadre_ModeleDirect.class.getResource("resources/images/JACKNIFFE.gif")));
        cmdTVariate.setIcon(new ImageIcon(Cadre_ModeleDirect.class.getResource("resources/images/TVariate.gif")));
        cmdEnd.setIcon(new ImageIcon(Cadre_ModeleDirect.class.getResource("resources/images/End.gif")));
        cmdPredict.setIcon(new ImageIcon(Cadre_ModeleDirect.class.getResource("resources/images/Prediction.gif")));
        cmdFitted.setToolTipText("Obs.-Fit. and Res. CPUE plots");
        cmdTVariate.setToolTipText("Three variate plots");
        cmdJackniffe.setToolTipText("Jackknife plots");
        cmdEnd.setToolTipText("Close (ESC)");
        int condition = JComponent.WHEN_IN_FOCUSED_WINDOW;
        KeyStroke keyStroke2 = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);
        InputMap inputMap2 = cmdEnd.getInputMap(condition);
        ActionMap actionMap2 = cmdEnd.getActionMap();
        inputMap2.put(keyStroke2, keyStroke2.toString());
        actionMap2.put(keyStroke2.toString(), new AbstractAction() {            
            @Override
            public void actionPerformed(ActionEvent arg0) {
                cmdEnd.doClick();
            }
        });
        cmdPredict.setToolTipText("Prediction");
        jToolBar1.add(cmdFitted, null);
        jToolBar1.add(cmdTVariate, null);
        jToolBar1.add(cmdJackniffe, null);
        jToolBar1.addSeparator();
        jToolBar1.add(cmdPredict, null);
        jToolBar1.addSeparator();
        jToolBar1.addSeparator();
        jToolBar1.add(cmdEnd, null);
        jToolBar1.setFloatable(false);

        cmdFitted.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                cmdFitted_actionPerformed(e);
            }
        });

        cmdTVariate.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                cmdTVariate_actionPerformed(e);
            }
        });

        cmdJackniffe.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                cmdJackniffe_actionPerformed(e);
            }
        });
        cmdEnd.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                cmdEnd_actionPerformed(e);
            }
        });

        cmdPredict.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                cmdPredict_actionPerformed(e);
            }
        });

        chkE.setSelected(true);

        /*ejecutar al abrir la ventana*/
        displayList(RechercheModele.getlisteModele(0));
        EnvironmentsetEnabled(false);
        Global.environmental_influence = Global.influenceEnv[0];
        Global.effort_preponderant = 1;
        /*fin ejecutar*/
    }

    void chkE_actionPerformed(ActionEvent e) {
        displayList(RechercheModele.getlisteModele(0));
        EnvironmentsetEnabled(false);
        Global.environmental_influence = Global.influenceEnv[0];
        Global.effort_preponderant = 1;
    }

    void chkV_actionPerformed(ActionEvent e) {
        displayList(RechercheModele.getlisteModele(1));
        EnvironmentsetEnabled(true);
        Global.environmental_influence = Global.influenceEnv[0];
        Global.effort_preponderant = 2;
    }

    void chkEVCatchability_actionPerformed(ActionEvent e) {
        displayList(RechercheModele.getlisteModele(2));
        EnvironmentsetEnabled(false);
        Global.environmental_influence = Global.influenceEnv[2];
        Global.effort_preponderant = -1;
    }

    void chkEVAbundance_actionPerformed(ActionEvent e) {
        displayList(RechercheModele.getlisteModele(3));
        EnvironmentsetEnabled(true);
        Global.effort_preponderant = -1;
        Global.environmental_influence = Global.influenceEnv[1];
    }

    void chkEVBoth_actionPerformed(ActionEvent e) {
        displayList(RechercheModele.getlisteModele(4));
        EnvironmentsetEnabled(true);
        Global.effort_preponderant = -1;
        Global.environmental_influence = Global.influenceEnv[3];
    }

    void cmdFitted_actionPerformed(ActionEvent e) {
        String windowTitre = "Model fitting: " + RechercheModele.getEquation() + "(fitting nb " + nbFit + ")";
        if (openWindow(windowTitre)) {
            return;
        }
        MsgDialogBox msg;
        Cadre_SplitPlot dlgSp;
        if (Global.modelisationOk) {
            dlgSp = new Cadre_SplitPlot(Global.fittedCpuePlot, true);
            dlgSp.setTitle(windowTitre);
            UtilCadre.Centrer(dlgSp);
            dlgSp.setVisible(true);
            vPlot.add(dlgSp);

        } else {
            msg = new MsgDialogBox(0, "Please,in order to display these plots\n you have to fit a model", 0, Global.CadreMain);
        }

    }

    ;
    void cmdTVariate_actionPerformed(ActionEvent e) {
        String windowTitre = "Model fitting: " + RechercheModele.getEquation() + "(fitting nb " + nbFit + ") ";
        if (openWindow(windowTitre)) {
            return;
        }
        MsgDialogBox msg;
        Cadre_SplitPlot dlgSp;
        if (Global.modelisationOk) {
            dlgSp = new Cadre_SplitPlot(Global.variatePlot);
            dlgSp.setTitle(windowTitre);
            UtilCadre.Centrer(dlgSp);
            dlgSp.setVisible(true);
            vPlot.add(dlgSp);
        } else {
            msg = new MsgDialogBox(0, "Please,in order to display these plots\n you have to fit a model", 0, Global.CadreMain);
        }
    }

    ;
    void cmdJackniffe_actionPerformed(ActionEvent e) {
        String windowTitre = "Model fitting: " + RechercheModele.getEquation() + "(fitting nb " + nbFit + ")  ";
        if (openWindow(windowTitre)) {
            return;
        }
        MsgDialogBox msg;
        Cadre_SplitPlot dlgSp;
        if (Global.validationOk) {
            dlgSp = new Cadre_SplitPlot(Global.jackknifePlot);
            dlgSp.setTitle(windowTitre);
            if (Global.nbre_param > 3) {
                UtilCadre.Size(dlgSp, 80, 90);
            }
            UtilCadre.Centrer(dlgSp);
            dlgSp.setVisible(true);
            vPlot.add(dlgSp);
        } else {
            msg = new MsgDialogBox(0, "Please,in order to display these plots\n you have to fit a model", 0, Global.CadreMain);
        }

    }

    ;

    void cmdPredict_actionPerformed(ActionEvent e) {
        MsgDialogBox msg;
        if (Global.modelisationOk) {
            if (Global.test_jackknife) {
                Cadre_Prediction dlg = new Cadre_Prediction(this);
                dlg.setTitle("Climprod: Prediction for the fitted model " + RechercheModele.getEquation());
                dlg.setModal(true);
                dlg.setVisible(true);
            } else {
                msg = new MsgDialogBox(0, "The model is not validated(bad jackknife test)\n You can't use it for prediction. ", 0, Global.CadreMain);
            }
        } else {
            msg = new MsgDialogBox(0, "Please,in order to do prediction\n you have to fit a model.", 0, Global.CadreMain);
        }
    }

    void cmdEnd_actionPerformed(ActionEvent e) {
        close();

    }

    ;

/*
    Affiche la liste des mod�les
*/

private void displayList(String[] liste) {

        jTextAreaInfoModel.setText("");
        modele = new DefaultListModel<String>();

        if (liste != null) {
            for (String element : liste) {
                modele.addElement(element);
            }
        }
        jlstChoix.setModel(modele);
    }

    /*
     S�lectionne un mod�le
     */
    void jlstChoix_valueChanged(ListSelectionEvent e) {
        String equation = (String) jlstChoix.getSelectedValue();
        if (equation != null) {
            RechercheModele.select(equation);
            Global.relationCPU_E = RechercheModele.getNumCpue_ERelation();
            Global.relationCPU_V = RechercheModele.getNumCpue_VRelation();
            ModelInformation();
        }
    }

    /*
     Affiche les informations sur le mod�le s�lectionn�.
     */
    private void ModelInformation() {
        jTextAreaInfoModel.setText("");
        InputStream file = Cadre_ModeleDirect.class.getResourceAsStream("resources/models/" + RechercheModele.getModelInformation());
        if (file == null) {
            return;
        }
   // String file=Configuration.helpPathName+Configuration.fileSep+RechercheModele.getModelInformation();
        // if(file.equals("")) return;
        try {

            String[] dataLine;
            ReadFileText rft = new ReadFileText(file);
            dataLine = rft.getLines();
            //System.out.println("help "+dataLine.length);
            if (dataLine == null) {
                jTextAreaInfoModel.setText("Help file " + file + " not found.");
            } else {
                for (String line : dataLine) {
                    if (line.trim().length() == 0) {
                        jTextAreaInfoModel.append("\n\n");
                    } else {
                        jTextAreaInfoModel.append(line + " ");
                    }
                }
                jTextAreaInfoModel.setCaretPosition(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void EnvironmentsetEnabled(boolean b) {

        jcboRecrutement.setEnabled(b);
        jcboBegining.setEnabled(b);
        jcboEnd.setEnabled(b);
        jlblRecrutement.setEnabled(b);
        jlblBegining.setEnabled(b);
        jlblEnd.setEnabled(b);
    }

    void cmdFit_actionPerformed(ActionEvent e) {
        MsgDialogBox msg;
        if (jlstChoix.getSelectedIndex() == -1) {
            msg = new MsgDialogBox(0, "Please, you have to select\n a model", 0, Global.CadreMain);
            return;
        }
        if (Global.CadreFittedPlots != null) {
            Global.CadreFittedPlots.dispose();
        }
        if (Global.CadreResidualPlots != null) {
            Global.CadreResidualPlots.dispose();
        }
        if (Global.CadreJackniffePlots != null) {
            Global.CadreJackniffePlots.dispose();
        }
        if (Global.CadreThreeVariatePlots != null) {
            Global.CadreThreeVariatePlots.dispose();
        }
        if (Global.CadreMSPlots != null) {
            Global.CadreMSPlots.dispose();
        }
        if (Global.CadreResultats != null) {
            Global.CadreResultats.dispose();
        }
        Global.recruitment_age = 0;
        Global.begin_influence_period = 0;
        Global.end_influence_period = 0;
        Global.modelisationOk = false;
        Global.nb_classes_exploitees = jcboExploitedYears.getSelectedIndex() + 1;
        if (jcboRecrutement.isEnabled()) {
            Global.recruitment_age = jcboRecrutement.getSelectedIndex() + 1;
            Global.begin_influence_period = jcboBegining.getSelectedIndex();
            Global.end_influence_period = jcboEnd.getSelectedIndex();
        }
        Global.numero_modele = RechercheModele.getNumero();
        Global.typeModele = RechercheModele.getType();
        Modele.Estimer();
        lblModele.setText("Model fitting: " + RechercheModele.getEquation() + " (Marquardt method)");
        renseignerTable(jTableModelisation, Modele.getResult());
        Validation.valide_modele();
        renseignerTable(jTableValidation, Validation.getParamResult());
        nbFit++;
        /* if(Global.nb_classes_exploitees==0)
         {
         msg=new MsgDialogBox(0,"Please, you have to give the number of significantly/n exploited year classes",0);
         return;
         }
         */

        /*if(Global.recruitment_age==0)
         {
         msg=new MsgDialogBox(0,"Please, you have to give the age at/n recrutement",0);
         return;
         }*/

        /* if(Global.begin_influence_period==-1)
         {
         msg=new MsgDialogBox(0,"Please, you have to give the age age/n at the begining of environmental influence",0);
         return;
         }*/

        /*if(Global.end_influence_period==-1)
         {
         msg=new MsgDialogBox(0,"Please, you have to give the age age/n at the end of environmental influence",0);
         return;
         }*/
          //popup.setBorderPainted(true);
        //popup.show(cmdFit,cmdFit.getWidth()-popup.getWidth(),0);
    }

    void this_windowClosing(WindowEvent e) {
        close();
    }

    private void renseignerTable(JTable t, Object[][] res) {
        if (res == null) {
            return;
        }
        int c = res[0].length;
        int l = res.length - 1;
        String[] column = new String[c];
        for (int j = 0; j < c; j++) {
            column[j] = (String) res[0][j];
        }

        Object[][] dat = new Object[l][c];
        for (int i = 0; i < l; i++) {
            System.arraycopy(res[i + 1], 0, dat[i], 0, c);

        }
        ModeleDeTableStat mdt = new ModeleDeTableStat(dat, column);
        t.setModel(mdt);
        TableColumn tmcolumn;
        int nbc = t.getColumnModel().getColumnCount();
        for (int i = 0; i < nbc; i++) {
            tmcolumn = t.getColumnModel().getColumn(i);
            tmcolumn.setMinWidth(50);
        }

    }

    private boolean openWindow(String windowTitre) {
        for (Object plot : vPlot) {
            Cadre_SplitPlot dlgSp = (Cadre_SplitPlot) plot;
            if (dlgSp != null) {
                if (dlgSp.getTitle().equals(windowTitre)) {
                    dlgSp.requestFocus();
                    if (dlgSp.getState() == Cadre_SplitPlot.ICONIFIED) {
                        dlgSp.setState(Cadre_SplitPlot.NORMAL);
                    }
                    return true;
                }
            }
        }
        return false;

    }

    private void close() {
        for (Object plot : vPlot) {
            Cadre_SplitPlot dlgSp = (Cadre_SplitPlot) plot;
            if (dlgSp != null) {
                dlgSp.dispose();
            }
        }
        vPlot.clear();
        nbFit = 0;
        this.dispose();
    }
}
