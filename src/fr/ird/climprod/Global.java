/**
 * Titre : Climprod<p>
 * Classe statique qui regroupe l'ensemble des variables globales
 *
 * @version 1.0
 */
package fr.ird.climprod;

import javax.swing.JFrame;
import java.util.HashMap;
import org.apache.commons.math3.special.Gamma;

public class Global {

    static final String[] influenceEnv = {"", "abundance", "catchability", "both"};//probl�me du both
    static JFrame CadreQuestion;                        //Une seule instance est permise pour l'instant
    static JFrame CadreModeleDirect;
    static JFrame CadreModele;
    static JFrame CadreResultats;
    static JFrame CadreTimePlots;
    static JFrame CadreBiVariatePlots;
    static JFrame CadreFittedPlots;
    static JFrame CadreResidualPlots;
    static JFrame CadreJackniffePlots;
    static JFrame CadreThreeVariatePlots;
    static JFrame CadreMSPlots;
    static JFrame CadreFaits;
    
    static JFrame CadreMain;
    /**
     * ******* Hypoth�ses de base *******
     */

    static boolean range_data;                            // Does  the  relative  range  of  observed  value<0.4
    static int changement_exploitation;                   // Have there been changes in the fishing pattern during the period (effort allocation, quota, mesh-size ...)
    static int sous_stock;                                // Does the data-set apply to a sub-stock
    static int stock_unique;                              // Does the data-set apply to a single stock
    static int stock_divise;                              // Is the single stock subdivided into various geographical sub-stocks (all must be exploited by the fleet)
    static int unite_standardisee;                        // Is the fishing effort unit standardized and  is the CPUE proportional to abundance
    static int effet_delais_abundance_negligeable;        // Do time-lags and  deviations from the stable age structure have negligible effects on production rate

    /**
     * ******* Stabilite de la mod�lisation *****
     */
    static int under_and_over_exploited;                  // Do you think that the data-set covers periods both of overexploitation and of underexploitation
    static int under_and_optimaly;                        // Do you think that the data-set cover periods both of underexploitation and optimal exploitation
    static int statistiques_anormales;                    // Did you see any abnormal statistics in the previous table
    static int unstability;                               // Is  interannual  variability  too  large
    static int independance;                              // Are  the  two  variables  independent
    static int abnormal_points_scatt;                     // Do  you  see  outlier  points
    static int abnormal_points_dist;                      // Do  you  see  outlier  points
    static int effort_increasing;                         // Constantly  increasing  effort

    static int decreasing_relationship;                   // Does  this  plot  appear  to  be  decreasing
    static int effort_preponderant;                       // Is the influence of fishing effort on CPUE more important than environmental influence

    /**
     * ************************************
     */
    static int climatic_influence;
    static int linear_relationship;                      // Does  this  plot  look  linear
    static int monotonic_relationship;                    // Does  this  plot  look  monotonic

    static int obviously;                                 // Does  this  plot  look  obviously  linear
    static int good_results;                              // Is  this  an  acceptable  model
    static int trend_residuals;                           // Good  fit  and  no  trend  or  autocorrelation  in  resid.

    static String environmental_influence;                // Does the environment influence:
    static int lifespan;                                  // What is the life span of the species
    static int nb_classes_exploitees;                     // Number of significantly exploited year-classes
    static int recruitment_age;                           // Age at recruitment
    static int begin_influence_period;                    // Age at the begining of environmental influence
    static int end_influence_period = 1;                    // Age at the end of environmental influence

    static int cpue_unstable;                             // Is there a strong instability in the cpue time series
    static int reserves_naturelles;                       // Are there natural protected areas for the stock or constantly inacessible adult biomass
    static int fecondite_faible;                          // Is the fecundity of the species very low (sharks, mammals, etc.)
    static int premiere_reproduction_avant_recrutement;   // Are there one or several non negligible spawnings before recruitment
    static int rapport_vie_exploitee_inferieur_deux;      // Is the ratio (lifespan/number of exploited year-classes) lower than 2
    static int sous_stock_isole;                          // Is the sub-stock well isolated, (i.e. with few exchanges) from others
    static int stock_deja_effondre;                       // Did the stock already collapse or exhibit drastic decrease(s) in catches
    static int pessimiste;                                // Do you have any (additional) reason to expect highly unstable behaviour or collapse of the stock

    static int cpue_sous_sur_production;                  // May the stock present large fluctuations in CPUE when overexploited

    static double jackknife;                              // Reasonable jackknife coefficient R2 (>65% recommended), no extreme yearly coefficient, and acceptable MSY graph
    static boolean test_jackknife;
    static int coeff_determination_instable;
    /**
     * *******Variables de travail ********
     */
    static boolean bavard = false;                          // Do you want to trace all the procedure of  rule application

    static String nom_fichier = null;                         //Name of the data file
    static int decalmax = 0;
    static int etapeEnCours = 0;                            //num�ro d'�tape dans la fixation du modele
    static final int questionM = 1;                         //Etape1
    static final int fixationM = 2;                         //Etape 2 recherche du mod�le
    static final int validationM = 3;                       //Etape 3 Validation du mod�le

    static String datafilePath = System.getProperty("user.home");

    /**
     * ************ Mod�le en cours *************
     */
    static int typeModele;                                // Type Simple-Climat-Mixte
    static int relationCPU_E;                             // Type relation entre CPU et E
    static int relationCPU_V;                             // Type relation entre CPU et V
    static int numero_modele;                             // Current model
    static int nbre_param;                                // Number of parameters
    static double[] val_param;                            // Value of parameters
    static double coeff_determination;                    //R2
    static double critereAIC;                             //Akeike
    static boolean validationOk;                          // true si Validation ex�cut�e
    static boolean modelisationOk;                        // true si mod�le ajust�

    /**
     * *********** Plots ************************
     */
//static int[]  typePlot = {2, 2, 2, 2, 3, 3, 3, 3, 1, 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 3, 2, 2,2,2,2,2};
//static Hashtable htPlot=new Hashtable();
    static String[] titreG = {"E Time Plot", "V Time Plot", "Y Time Plot", "CPUE Time Plot", "E Distribution", "V Distribution", "Y Distribution", "CPUE Distribution", "Y versus E", "Y versus V", " CPUE versus E ", "CPUE versus V", "E versus V", "Observed and Fitted CPUE", "Time Plot of residual CPUE", "Residual CPUE versus E", "Residual CPUE versus V", "a (%) Time Plot", " b (%) Time Plot", "c (%) Time Plot", "d (%) Time", "Plot R2 (% of %) Time Plot", "Function Y versus V & E", "Function Y versus V & E", "Function CPUE versus V & E", "Function CPUE versus V & E", "MSY versus V", "MS-E versus V"};
    static String[] titreSx = {"Year", "Year", "Year", "Year", "Classes", "Classes", "Classes", "Classes", "Effort (E)", "Environment (V)", "Effort (E)", "Environment (V)", "Environment (V)", "Year", "Year", "Effort (E)", "Environment (V)", "Year", "Year", "  Year", "Year", "Year", "Effort (E)", "Effort (E)", "Effort (E)", " Effort (E)", "Environment (V)", "Environment (V)"};
    static String[] titreSy = {"Effort (E)", "Environment (V)", "Production (Y)", "CPUE ", "Nb", "Nb", "Nb", "Nb", "Production (Y)", "Production (Y)", "CPUE", "CPUE", "CPUE", "CPUE", "    Residual", "Residual", "Residual", "a (%)", "b (%)", "c (%)", "d (%)", "R² (%)", "Production (Y)", "Production (Y)", "CPUE", "CPUE", "MSY", "MS-E"};

    static Plot[] timePlot = new Plot[4];
    static PlotModal[] distriPlot = new PlotHisto[4];
    static Plot[] scatterPlot = new Plot[4];
    static Plot[] lagPlot = new Plot[2];
    static Plot indePlot = new Plot();
    static Plot[] fittedCpuePlot = new Plot[2];
    static Plot[] residualsPlot = new Plot[2];
    static PlotHisto[] jackknifePlot;
    static Plot[] variatePlot = new Plot[2];
    static Plot[] msyPlot = new Plot[2];
    static HashMap<Integer, String> questionDic = new HashMap<Integer, String>();
    static HashMap<Integer, String> answerDic = new HashMap<Integer, String>();
    static HashMap<Integer, String> warningDic = new HashMap<Integer, String>();

    public static void init() {

        if (CadreModeleDirect != null) {
            CadreModeleDirect.dispose();
        }
        if (CadreModele != null) {
            CadreModele.dispose();
        }
        if (CadreQuestion != null) {
            CadreQuestion.dispose();
        }
        if (CadreResultats != null) {
            CadreResultats.dispose();
        }
        if (CadreBiVariatePlots != null) {
            CadreBiVariatePlots.dispose();
        }
        if (CadreTimePlots != null) {
            CadreTimePlots.dispose();
        }
        if (CadreFittedPlots != null) {
            CadreFittedPlots.dispose();
        }
        if (CadreResidualPlots != null) {
            CadreResidualPlots.dispose();
        }
        if (CadreJackniffePlots != null) {
            CadreJackniffePlots.dispose();
        }
        if (CadreThreeVariatePlots != null) {
            CadreThreeVariatePlots.dispose();
        }
        if (CadreMSPlots != null) {
            CadreMSPlots.dispose();
        }

        range_data = false;              // Does  the  relative  range  of  observed  value<0.4
        changement_exploitation = -1; // Have there been changes in the fishing pattern during the period (effort allocation, quota, mesh-size ...)
        sous_stock = -1;              // Does the data-set apply to a sub-stock
        stock_unique = -1;            // Does the data-set apply to a single stock
        stock_divise = -1;            // Is the single stock subdivided into various geographical sub-stocks (all must be exploited by the fleet)
        unite_standardisee = -1;      // Is the fishing effort unit standardized and  is the CPUE proportional to abundance
        effet_delais_abundance_negligeable = -1; //	Do time-lags and  deviations from the stable age structure have negligible effects on production rate

        /**
         * ******* Stabilite de la mod�lisation *****
         */
        under_and_over_exploited = -1;// Do you think that the data-set covers periods both of overexploitation and of underexploitation
        under_and_optimaly = -1;      // Do you think that the data-set cover periods both of underexploitation and optimal exploitation
        statistiques_anormales = -1;  // Did you see any abnormal statistics in the previous table
        unstability = -1;             // Is  interannual  variability  too  large
        independance = -1;            // Are  the  two  variables  independent
        abnormal_points_scatt = -1;   // Do  you  see  outlier  points
        abnormal_points_dist = -1;    // Do  you  see  outlier  points
        effort_increasing = -1;       // Constantly  increasing  effort

        decreasing_relationship = -1; // Does  this  plot  appear  to  be  decreasing
        effort_preponderant = -1;     // Is the influence of fishing effort on CPUE more important than environmental influence

        /**
         * ************************************
         */
        climatic_influence = 1;
        linear_relationship = -1;     // Does  this  plot  look  linear
        monotonic_relationship = -1;  // Does  this  plot  look  monotonic

        obviously = -1;               // Does  this  plot  look  obviously  linear
        good_results = -1;            // Is  this  an  acceptable  model
        trend_residuals = -1;         // Good  fit  and  no  trend  or  autocorrelation  in  resid.

        environmental_influence = "";     // Does the environment influence:
        lifespan = -1;                       // What is the life span of the species
        nb_classes_exploitees = -1;//1;           // Number of significantly exploited year-classes
        recruitment_age = 0;//0;                 // Age at recruitment
        begin_influence_period = 0;          // Age at the begining of environmental influence
        end_influence_period = 0;//0;            // Age at the end of environmental influence

        cpue_unstable = -1;           // Is there a strong instability in the cpue time series
        reserves_naturelles = -1;     // Are there natural protected areas for the stock or constantly inacessible adult biomass
        fecondite_faible = -1;        // Is the fecundity of the species very low (sharks, mammals, etc.)
        premiere_reproduction_avant_recrutement = -1; //	Are there one or several non negligible spawnings before recruitment
        rapport_vie_exploitee_inferieur_deux = -1; //	Is the ratio (lifespan/number of exploited year-classes) lower than 2
        sous_stock_isole = -1; //	Is the sub-stock well isolated, (i.e. with few exchanges) from others
        stock_deja_effondre = -1; //	Did the stock already collapse or exhibit drastic decrease(s) in catches
        pessimiste = -1; //	Do you have any (additional) reason to expect highly unstable behaviour or collapse of the stock

        cpue_sous_sur_production = -1; //	May the stock present large fluctuations in CPUE when overexploited

        jackknife = 0.0d; //	Reasonable jackknife coefficient R2 (>65% recommended), no extreme yearly coefficient, and acceptable MSY graph
        test_jackknife = false;
        coeff_determination_instable = -1;
        /**
         * *******Variables de travail ********
         */

        decalmax = 0;
        etapeEnCours = 0;                    //num�ro d'�tape dans la fixation du modele

        /**
         * ************ Mod�le en cours *************
         */
        typeModele = 0;               // Type Simple-Climat-Mixte
        relationCPU_E = 0;           // Type relation entre CPU et E
        relationCPU_V = 0;           // Type relation entre CPU et V
        numero_modele = -2;           // Current model
        nbre_param = 0;               // Number of parameters
        coeff_determination = 0;   //R2
        critereAIC = 0;
        modelisationOk = false;
        validationOk = false;     // true si Validation ex�cut�e
//bavard=true;
        questionDic.clear();
        answerDic.clear();
        warningDic.clear();
    }
    
    public static double gam(double x){
        return(Gamma.gamma(x));
    }
    
    // f(x) = Γ((n1 + n2)/2) / (Γ(n1/2) Γ(n2/2)) (n1/n2)^(n1/2) x^(n1/2 - 1) (1 + (n1/n2) x)^-(n1 + n2)/2
    public static double pfisher(double x, double n1, double n2){
        //double n1 = 2;
        //double n2 = 9;
        return(
                gam((n1 + n2)/2) /
                (gam(n1/2) * gam(n2/2)) *
                java.lang.Math.pow( (n1/n2), (n1/2) ) *
                java.lang.Math.pow(x, (n1/2) - 1 ) *
                java.lang.Math.pow(1 + (n1/n2)* x, -(n1 + n2)/2)
            );
    }
    
    // F=(R²(n-p-1)) / (p(1-R²)
    // F = (r_tot*(nim-nbre_par-1))/(nbre_par*(1-r_tot))
    public static double fF(double R2, double n, double p){
        return(
                (R2*(n-p-1)) / (p*(1-R2))
            );
    }
}
