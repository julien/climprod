
/**
 * Titre : Climprod<p>

 */
package fr.ird.climprod;

public class OnError extends Exception {

  public OnError() {
  }
  public OnError(String s) {
    super(s);
  }
}