/**
 * Titre : Climprod<p>
 */
package fr.ird.climprod;

import javax.swing.*;
import java.awt.*;
import javax.swing.table.TableColumn;
import java.awt.event.*;
import javax.swing.border.TitledBorder;
import java.io.InputStream;

//public class Cadre_Modele extends JFrame {
public class Cadre_Modele extends JDialog {

    JScrollPane jScrollPane1 = new JScrollPane();
    JScrollPane jScrollPane3 = new JScrollPane();
    JTextArea jTextAreaModele = new JTextArea();
    JPanel jPanResult = new JPanel();
    JPanel jPanCmd = new JPanel();
    JButton cmdNext = new JButton();
    GridBagLayout gridBagLayout1 = new GridBagLayout();
    JTable jTabResult = new JTable();
    BorderLayout borderLayout1 = new BorderLayout();
    JScrollPane jScrollPane2 = new JScrollPane();
    JTextArea jTextAreaInfo = new JTextArea();
    BorderLayout borderLayout2 = new BorderLayout();
    
    private JFrame parent;

  //JFrame frc;
    public Cadre_Modele(JFrame frame) {
        super(frame, "", true);
        enableEvents(AWTEvent.WINDOW_EVENT_MASK);
        try {
            //frc=frame;
            this.parent = frame;
            initWindow();
            renseignerModele();
            //lblModele.setText("Model fitting: "+ RechercheModele.getEquation()+ " (Marquardt method)");
            renseignerTable(jTabResult, Modele.getResult());
            UtilCadre.Size(this, 100, 90, true);
            UtilCadre.Centrer(this);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void initWindow() throws Exception {
    //this.setIconImage(Toolkit.getDefaultToolkit().createImage(Cadre_Modele.class.getResource("Climprod.jpg")));
        //this.setTitle("Climprod: Model fitting");
        this.addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                Global.CadreModele = null;
            }
        });
        this.getContentPane().setLayout(gridBagLayout1);

        jTextAreaModele.setLineWrap(true);
        jTextAreaModele.setWrapStyleWord(true);
        jTextAreaModele.setText("");
        jTextAreaInfo.setLineWrap(true);
        jTextAreaInfo.setWrapStyleWord(true);

        cmdNext.setText("Next (N)");
        int condition = JComponent.WHEN_IN_FOCUSED_WINDOW;
        KeyStroke keyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_N, 0);
        InputMap inputMap = cmdNext.getInputMap(condition);
        ActionMap actionMap = cmdNext.getActionMap();
        inputMap.put(keyStroke, keyStroke.toString());
        actionMap.put(keyStroke.toString(), new AbstractAction() {            
            @Override
            public void actionPerformed(ActionEvent arg0) {
                cmdNext.doClick();
            }
        });
        cmdNext.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                cmdNext_actionPerformed(e);
            }
        });

        jPanResult.setLayout(borderLayout1);
        jPanCmd.setLayout(borderLayout2);
        jScrollPane2.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(142, 142, 142)), "Model information"));
        jScrollPane3.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(142, 142, 142)), "Main statistical results"));
        jScrollPane2.getViewport().add(jTextAreaInfo, null);
        jScrollPane1.getViewport().add(jTextAreaModele, null);
        jScrollPane3.getViewport().add(jTabResult, null);

        this.getContentPane().add(jScrollPane1, new GridBagConstraints(0, 0, 2, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(4, 5, 0, 5), 322, 20));
        this.getContentPane().add(jScrollPane3, new GridBagConstraints(0, 2, 2, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(10, 5, 10, 5), 369, 50));
        //jPanResult.add(jTabResult, BorderLayout.CENTER);
        this.getContentPane().add(jPanCmd, new GridBagConstraints(0, 4, 2, 1, 1.0, 1.0, GridBagConstraints.SOUTH, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 6, 5), 167, 7));
        jPanCmd.add(cmdNext, BorderLayout.CENTER);
        this.getContentPane().add(jScrollPane2, new GridBagConstraints(0, 3, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 70));

    }

    private void renseignerTable(JTable t, Object[][] res) {
        if (res == null) {
            return;
        }
        int c = res[0].length;
        int l = res.length - 1;
        String[] column = new String[c];
        for (int j = 0; j < c; j++) {
            column[j] = (String) res[0][j];
        }

        Object[][] dat = new Object[l][c];
        for (int i = 0; i < l; i++) {
            for (int j = 0; j < c; j++) {
                dat[i][j] = res[i + 1][j];
            }

        }
        ModeleDeTableStat mdt = new ModeleDeTableStat(dat, column);
        t.setModel(mdt);
        TableColumn tmcolumn = null;
        int nbc = t.getColumnModel().getColumnCount();
        for (int i = 0; i < nbc; i++) {
            tmcolumn = t.getColumnModel().getColumn(i);
            tmcolumn.setMinWidth(50);
        }

    }

    private void renseignerModele() {

        switch (RechercheModele.getType()) {

            case RechercheModele.effort:
                jTextAreaModele.setText("Knowing the relationship between Cpue and E is " + RechercheModele.getCpue_ERelation());
                jTextAreaModele.append("\n\t");
                jTextAreaModele.append("A possible Conventional models CPUE=f(E) could be " + RechercheModele.getEquation());
                break;
            case RechercheModele.climat:
                jTextAreaModele.setText("Knowing the relationship between Cpue and V is " + RechercheModele.getCpue_VRelation());
                jTextAreaModele.append("\n\t");
                jTextAreaModele.append("A possible Conventional models CPUE=f(V) could be " + RechercheModele.getEquation());
                break;
            case RechercheModele.mixte:
                if (Global.effort_preponderant == 1) {
                    jTextAreaModele.setText("Knowing the relationship between Cpue and E is " + RechercheModele.getCpue_ERelation());
                    jTextAreaModele.append("\n");
                    jTextAreaModele.append("Knowing the environmental effect is  " + Global.environmental_influence);
                    jTextAreaModele.append("\n\t");
                    jTextAreaModele.append("A possible global model  could be " + RechercheModele.getEquation());
                    jTextAreaModele.append("\n\t");
                    jTextAreaModele.append("And the rule governing the relationship between residual Cpue and V is " + RechercheModele.getCpue_VRelation());
                } else {
                    jTextAreaModele.setText("Knowing the relationship between Cpue and V is " + RechercheModele.getCpue_VRelation());
                    jTextAreaModele.append("\n");
                    jTextAreaModele.append("Knowing the environmental effect is  " + Global.environmental_influence);
                    jTextAreaModele.append("\n\t");
                    jTextAreaModele.append("A possible global model  could be " + RechercheModele.getEquation());
                    jTextAreaModele.append("\n\t");
                    jTextAreaModele.append("And the rule governing the relationship between residual Cpue and E is " + RechercheModele.getCpue_ERelation());
                }
                break;
            default:
                jTextAreaModele.setText("no convenient model found\n\nThere is no available model appropriate to your case");
                  //cmdFit.setEnabled(false);
            //cmdNext.setEnabled(true);

        }
        ModelInformation();

    }

    private void ModelInformation() {
        jTextAreaInfo.setText("");
        InputStream file = Cadre_Modele.class.getResourceAsStream("resources/models/" + RechercheModele.getModelInformation());
   // String file=Configuration.helpPathName+Configuration.fileSep+RechercheModele.getModelInformation();
        // if(file.equals("")) return;
        if (file == null) {
            return;
        }
        try {

            String[] dataLine;
            ReadFileText rft = new ReadFileText(file);
            dataLine = rft.getLines();
            //System.out.println("help "+dataLine.length);
            if (dataLine == null) {
                jTextAreaInfo.setText("Help file " + file + " not found.");
            } else {
                for (int i = 0; i < dataLine.length; i++) {
                    if (dataLine[i].trim().length() == 0) {
                        jTextAreaInfo.append("\n\n");
                    } else {
                        jTextAreaInfo.append(dataLine[i] + " ");
                    }
                }
            }
            jTextAreaInfo.setCaretPosition(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void cmdNext_actionPerformed(ActionEvent e) {
        dispose();
        this.parent.toFront();
    }

    @Override
    protected void processWindowEvent(WindowEvent e) {
        if (e.getID() == WindowEvent.WINDOW_CLOSING) {
            dispose();
        }
        super.processWindowEvent(e);
    }

}
