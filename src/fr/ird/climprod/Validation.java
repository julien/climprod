
/**
 * Titre : Climprod<p>
*/
package fr.ird.climprod;
import static fr.ird.climprod.Data.getNbDataRetenue;
import java.text.DecimalFormat;
import java.awt.Color;
import java.io.*;
public class Validation {
static private int nim;
      static private int nparmax=4,nv=50;
      static private int jk,nbre_par;
      static private int riter,iter,ritertot,maxrit=100;
      static private double 	val_init,val_fin,   alpha=4,ecart;
      static private double sompar,variance_pue,eps=1.E-8,mul=1.E-3,yy,r_jk, r_tot;

      static private double[] ptmp=new double[nparmax+2];
      static private double[][] covar=new double[nparmax+1][nparmax+1];

      static private double[][] hessien=new double[nparmax+1][nparmax+1];
      static private double[] covpy=new double[nparmax+2];
      static private double[] par_alors=new double[nparmax+2];
      static private double[] racineh=new double[nparmax+2];
      static private double[] derivee=new double[nparmax+2];
      static private double[] delta=new double[nparmax+2];
      static private double[] par_init=new double[nparmax+2];
      static private double[] par_inter=new double[nparmax+2];
      static private double[] spar=new double[nparmax+2];
      static private double[] par=new double[nparmax+2];
      static private double[] dpar=new double[nparmax+2];
      static private double[] sompari=new double[nparmax+2];
      static private double[] f,v,vbar,pue,res;
      static private double[]  resiter=new double [maxrit];
      static private double[][] derfonc=new double [61][nparmax+1];
      static private double[][] jkn=new double [61][nparmax+1];
      static private double[][] pseudo=new double [61][nparmax+1];
      static private double[] val_v=new double[4];
      static private double[] moyenne_pseudo=new double[nparmax+2];
      static private double[][] covar_pseudo=new double[nparmax+1][nparmax+1];
      static private double[] ecart_pseudo=new double[nparmax+2];

      static private         double []f_ms_m=new double[nv];
      static private         double []f_ms_s=new double[nv];
      static private         double []y_ms_m=new double[nv];
      static private         double []y_ms_s=new double[nv];
      static private 	     double []vec_y_v=new double[nv];
      static private         double []vec_y_min=new double[nv];
      static private         double []vec_y_max=new double[nv];
      static private         double []vec_f_v=new double[nv];
      static private         double []vec_f_min=new double[nv];
      static private         double []vec_f_max=new double[nv];

static private double[]f_ms_m2=new double[nv];
static private double []vec_f_min2=new double[nv];
static private         double []vec_f_max2=new double[nv];
static private double[]y_ms_m2=new double[nv];
static private       double []vec_y_min2=new double[nv];
static private         double []vec_y_max2=new double[nv];

private static void marquardt()
{
        int 	iter=0,riter=0,i,j,k;
	double 	c,s,val_fin,val_init,ecart=100.0,alpha=4,lambda=16,rl =1.5;

        double[] val_iter=new double[maxrit];
	for(i=0;i<nbre_par;i++) par_alors[i] = par_init[i];

	//ritertot=0;
	val_fin=somme_residus_jk(par_alors);
        //System.out.println("valfin  "+val_fin);
	//resiter[0]=val_fin;
	//ecart= 100;
	//riter=0;
	while	(
			(iter<maxrit-1) &&
			((ecart > 0.05) || (iter<40)) &&
			(ecart  > 0.0001) &&
			(riter  > (-17))
		) {
		iter++;
                val_init = val_fin;
		calcul_derivee_hess();
                for(i=0;i<nbre_par;i++) hessien[i][i] += eps;
		for(i=0;i<nbre_par;i++) racineh[i] = Math.sqrt(hessien[i][i]);
		for(i=0;i<nbre_par;i++) derivee[i] /= racineh[i];
		for(i=0;i<nbre_par;i++) for(j=0;j<nbre_par;j++)
			hessien[i][j] /= (racineh[i]*racineh[j]);
		for(i=0;i<nbre_par;i++) hessien[i][i] += lambda;
                //for(i=0;i<nbre_par;i++)
                   // System.out.println("rac "+racineh[i]+" deri " +derivee[i] );
                //*******************************************
		//inverse_gauss( nbre_par, hessien, delta, derivee );
                  for(i=0;i<nbre_par-1;i++) {
	              for(j=i+1;j<nbre_par;j++){
		        c=hessien[j][i]/hessien[i][i];
		      for(k=i+1;k<nbre_par;k++) hessien[j][k]=hessien[j][k]-c*hessien[i][k];
    		      derivee[j]=derivee[j]-c*derivee[i];
	              }
                  }
                  delta[nbre_par-1] = -derivee[nbre_par-1]/hessien[nbre_par-1][nbre_par-1];
                  for(i=nbre_par-2;i>=0;i--){
	                s=derivee[i];
	                for(k=i+1;k<nbre_par;k++) s += hessien[i][k]*delta[k];//bizare
	                delta[i]= -s/hessien[i][i];
                  }
                  for(i=0;i<nbre_par;i++) delta[i] = - delta[i];
               //
		for(i=0;i<nbre_par;i++)
                {
                // System.out.println("delta "+ delta[i]);
                 delta[i] /= racineh[i];
                }
		for(i=0;i<nbre_par;i++) par_inter[i] = par_alors[i]-delta[i];
		val_fin = somme_residus_jk(par_inter);
                //System.out.println("valfin "+ iter+" " +val_fin);
		riter=0;
		if (val_fin < val_init) {
			while (val_fin < val_init) {
				val_init = val_fin;
				for(i=0;i<nbre_par;i++){
					delta[i] *= alpha;
					par_inter[i] = par_alors[i] - delta[i];
					}
				val_fin = somme_residus_jk(par_inter);
				riter++;
				//ritertot++;
				}
			for(i=0;i<nbre_par;i++) {
				delta[i] /= alpha;
				par_inter[i] = par_alors[i] - delta[i];
				}
                        riter--;
			val_fin = somme_residus_jk(par_inter);
			}
		else {
			while (val_fin > val_init)  {
				for(i=0;i<nbre_par;i++){
					delta[i] /= alpha;
					par_inter[i] = par_alors[i]-delta[i];
					}
				val_fin = somme_residus_jk(par_inter);
				riter--;
				if (riter < (-10)) for(i=0;i<nbre_par;i++) delta[i]=0;
				//ritertot++;
				}
			}
		val_iter[iter]=val_fin;
		if ( riter<0) 	lambda *=rl;
			else 	lambda /=rl;
		if(iter>5) ecart= 100*(val_iter[iter-5] - val_fin)/val_iter[iter-5];

		for(i=0; i<nbre_par; i++) par_alors[i] = par_inter[i];
		}
	}

private static void calcul_derivee_hess()
{
	int 	i,j,n;
	double 	v1,v2;
	for(i=0;i<nbre_par;i++) {
		par[i] 	= par_alors[i];
		dpar[i] = mul*par[i];
		}
	for(n=0;n<nim;n++) {
                if(n !=jk)
                {
		v1 = EquationModele.fonction_modele(f[n],v[n],vbar[n],par);
                //System.out.println("v1 " +n+ "  "+ v1);
		res[n] = v1 - pue[n];
		for(i=0;i<nbre_par;i++) {
			par[i] += dpar[i];
			v2 = EquationModele.fonction_modele(f[n],v[n],vbar[n],par);
                        //System.out.println("v2 " +i+ "  "+ v2);
			derfonc[n][i] = (v2 - v1)/dpar[i];
			par[i] -= dpar[i];
			}
		}
        }
	for(i=0;i<nbre_par;i++) {
			derivee[i] = 0;
			for(n=0;n<nim;n++) if(n !=jk)
				derivee[i] += (res[n]*derfonc[n][i]);
                        //System.out.println("derive " +i+ "  "+ derivee[i]);

			}
	for(i=0;i<nbre_par;i++)
		for(j=0;j<=i;j++){
			hessien[i][j]= eps;
			for(n=0;n<nim;n++) if(n !=jk)
				hessien[i][j] += (derfonc[n][i]*derfonc[n][j]);
			hessien[j][i] = hessien[i][j];
                         //System.out.println("hessien " +i+ "  "+ hessien[i][j]);
			}

	}

private static double somme_residus_jk(double[] par)
{
	double cc=0;
	int i;
	for(i=0;i<nim;i++)if(i !=jk)
        {
          double c= EquationModele.fonction_modele(f[i],v[i],vbar[i],par) - pue[i];
          cc += (c*c);
        }
	return(cc);
}

private static void calc_var_jk()
{

 int 	i;
	double 	s1=0,s2=0;
	for(i=0;i<nim;i++) if(i != jk) {
		s1 += pue[i];
		s2 += (pue[i]*pue[i]);
		}
	if (jk != -1) variance_pue = s2 - (s1*s1)/(double)(nim-1);
		else variance_pue = s2 - (s1*s1)/(double)nim;
}

public static void valide_modele()
{
	int 	i,j,k,l,iv;
        //int nv=50;
	double  var_tot, sdc_tot, sdc_jk;
	double	f_ms,y_ms,vmin,vmax,vv,ecartv;
        double[] pue_c,pue_j;
       /* double []f_ms_m=new double[nv];
        double []f_ms_s=new double[nv];
        double []y_ms_m=new double[nv];
        double []y_ms_s=new double[nv];
	double []vec_y_v=new double[nv];
        double []vec_y_min=new double[nv];
        double []vec_y_max=new double[nv];
        double []vec_f_v=new double[nv];
        double []vec_f_min=new double[nv];
        double []vec_f_max=new double[nv];*/
       	double f_ms2;

        double[]f_ms_s2=new double[nv];
        double y_ms2;

        double[]y_ms_s2=new double[nv];
	double []vec_y_v2=new double[nv];

	double []vec_f_v2=new double[nv];

	double[]trjk=new double[nv];
	//char 	cc[2];
	//char	*nom_jk[] = {"R2(%) time plot","a(%) time plot","b(%) time plot","c(%) time plot","d(%) time plot"};
	//char val[25];
	/**************** initialisations ********************/
          nbre_par=Global.nbre_param;
          for(i=0;i<nbre_par;i++) par_init[i] = Global.val_param[i];

          nim=Data.getNbDataRetenue();
          res= new double[nim];
          pue_c=new double[nim];
          pue_j=new double[nim];
          f=Data.getF();
          v=Data.getV();
          vbar=Data.getVbar();
          pue=Data.getPue();
	  if((Global.numero_modele > 5) || (Global.numero_modele< 2))
		for(iv=0;iv<nv;iv++){
			f_ms_m[iv] = 0.0;
			f_ms_s[iv] = 0.0;
			y_ms_m[iv] = 0.0;
			y_ms_s[iv] = 0.0;
			f_ms_m2[iv] = 0.0;
			f_ms_s2[iv] = 0.0;
			y_ms_m2[iv] = 0.0;
			y_ms_s2[iv] = 0.0;

			}
        double[] ext=Stat1.Extremas(v);
	val_v[2] = ext[0];
	val_v[3] = ext[1];
        ext=Stat1.quant(v,3);
        val_v[0]=Stat1.moy(v);
        val_v[1]=ext[1];
	ecartv = (val_v[3] - val_v[2])/nv;


	jk = -1;
	calc_var_jk();
	var_tot = variance_pue;
	sdc_tot = somme_residus_jk(par_init);
	r_tot = 100*(1 - (sdc_tot/var_tot));
	sdc_jk  = 0;
	//for(i=0;i<nbre_par;i++) System.out.print(par_init[i]+"  ");
	//System.out.println("R²  "+r_tot);
	/**************** boucle jackknife ******************/
	for(jk=0;jk<nim;jk++){

		calc_var_jk();
		marquardt();
		pue_c[jk] = EquationModele.fonction_modele(f[jk],v[jk],vbar[jk],par_init);
		pue_j[jk] = EquationModele.fonction_modele(f[jk],v[jk],vbar[jk],par_alors);
		sdc_jk += (pue[jk]-pue_j[jk])*(pue[jk]-pue_j[jk]);
		for(i=0;i<nbre_par;i++) jkn[jk][i+1]=par_alors[i];//ici
		val_fin = somme_residus_jk(par_alors);
		jkn[jk][0] = 1 - (val_fin/variance_pue);//pour r�
		for(i=0;i<nbre_par;i++)
		  pseudo[jk][i]= nim*par_init[i]-(nim-1)*par_alors[i];//ici
		vv = val_v[2];
		if((Global.numero_modele > 5) || (Global.numero_modele < 2))
		for(iv=0;iv<nv;iv++){
				f_ms = EquationModele.minimum_fonction(vv,par_alors);
				y_ms = f_ms * EquationModele.fonction_modele(f_ms,vv,vv,par_alors);
				f_ms_m[iv] += f_ms;
				f_ms_s[iv] += (f_ms*f_ms);
				y_ms_m[iv] += y_ms;
				y_ms_s[iv] += (y_ms*y_ms);
				vv += ecartv;
				}
	    //******************************************************************
	    //*** Calcul des MSY MSE pour les valeurs remarquables**************
	    //******************Modifications du 20/10/96***********************
		for(iv=0;iv<4;iv++){
		        	vv = val_v[iv];
				f_ms2 = EquationModele.minimum_fonction(vv,par_alors);
				y_ms2 = f_ms2 * EquationModele.fonction_modele(f_ms2,vv,vv,par_alors);
				f_ms_m2[iv] += f_ms2;
				f_ms_s2[iv] += (f_ms2*f_ms2);
				y_ms_m2[iv] += y_ms2;
				y_ms_s2[iv] += (y_ms2*y_ms2);
		}
	    //*******************************************************************

		if(jkn[jk][0]<0) jkn[jk][0] = 0.0;
               // for(i=0;i<nbre_par;i++) System.out.print(par_alors[i]+"   ");
		//System.out.println("R�  " +100*jkn[jk][0]);
	}

	/**************** calculs des pseudo-valeurs *********************/
       	for(i=0;i<nbre_par;i++)  {
		moyenne_pseudo[i] = 0.0;//ici
		for(jk=0;jk<nim;jk++)
			moyenne_pseudo[i] += pseudo[jk][i]; //ici 2
		moyenne_pseudo[i] /= (double)nim;//ici
	}
	for(i=0;i<nbre_par;i++) for(j=0;j<nbre_par;j++) { //bizae
		covar_pseudo[i][j] = 0.0;//ici
		for(jk=0;jk<nim;jk++)
			covar_pseudo[i][j] += pseudo[jk][i] * pseudo[jk][j];//ici 2 (i+1 2fois)
		covar_pseudo[i][j] /= (double)nim;//ici
		covar_pseudo[i][j] -= (moyenne_pseudo[i]*moyenne_pseudo[j]);//ici
		covar_pseudo[i][j] /= (double)nim;//ici
                //System.out.println(i+"  "+j+"covar   "+covar_pseudo[i][j]);
	}
	for(i=0;i<nbre_par;i++)  ecart_pseudo[i] = Math.sqrt(covar_pseudo[i][i]);
        /****************************** Test du Jackknife *******************************/
        Global.test_jackknife=true;
        // TODO vérifier et décommenter
        for(i=0;i<nbre_par;i++) {
		if ( Math.abs(par_init[i]/ecart_pseudo[i]) < 2.00) Global.test_jackknife=false;
	}
        // TODO add fisher test
        /****************************************************************************/
	r_jk  = (1 - (sdc_jk/var_tot));
	if(r_tot<0.0) r_tot = 0.0;
	if(r_jk<0.0) r_jk = 0.0;
        //System.out.println(" Conventional coefficient of determination R² = " + r_tot);
	//System.out.println("    Jackknife coefficient of determination R² = "+r_jk);
        Global.coeff_determination=r_tot/100;
        Global.jackknife=r_jk;
        for(i=0;i<nbre_par;i++)
        {
	  for(jk=0;jk<nim;jk++)
            	  	trjk[jk] = 100*jkn[jk][i+1]/par_init[i];//ici
               // System.out.print(trjk[jk]+"  ");

         // System.out.println();
        }

		/*******Calcul des valeurs th�oriques en fonction de V observ�**********/
		/******** Modification du 20/10/96*******************/

			for(iv=0;iv<4;iv++){
			vv = val_v[iv];
			f_ms_m2[iv] /= nim;
			f_ms_s2[iv] /= nim;
			f_ms_s2[iv] -= (f_ms_m2[iv]*f_ms_m2[iv]);//sqr
			if(f_ms_s2[iv]>0.0)f_ms_s2[iv] = Math.sqrt(f_ms_s2[iv]);
			else f_ms_s2[iv] = 0.0;
			vec_f_min2[iv] = f_ms_m2[iv] - 2.00 * f_ms_s2[iv];
			vec_f_max2[iv] = f_ms_m2[iv] + 2.00 * f_ms_s2[iv];
			y_ms_m2[iv] /= nim;
			y_ms_s2[iv] /= nim;
			y_ms_s2[iv] -= (y_ms_m2[iv]*y_ms_m2[iv]);//sqr
			if(y_ms_s2[iv]>0.0)y_ms_s2[iv] = Math.sqrt(y_ms_s2[iv]);
			else y_ms_s2[iv] = 0.0;
			vec_y_min2[iv] = y_ms_m2[iv] - 2.00 * y_ms_s2[iv];
			vec_y_max2[iv] = y_ms_m2[iv] + 2.00 * y_ms_s2[iv];
			}

                //saveResult();
                makePlot();
                Global.validationOk=true;


}
static private void makePlot(){
/***********jacknife plot **************************/
String[] ty={"a","b","c","d"};
double[] years=Data.getYears();
String[] etiq$=new String[nim];
double[] etiq=new double[nim];
double[] serie100=new double[nim];


DecimalFormat df=new DecimalFormat("0");
for(int i=0;i<nim;i++)
{
        etiq$[i]=df.format(years[i]).toString();
        etiq[i]=i+1;
}
for(int i=0;i<nim;i++)	serie100[i] = 100;
PlotSerie ps100=new  PlotSerie("100%",etiq,"100%",serie100);
ps100.setFigure(2);
ps100.setCouleur(Color.black);
//PlotSerie[] ps=new PlotSerie[nbre_par+1];
Global.jackknifePlot= new PlotHisto[nbre_par+1];
double[] trjk;
for(int i=0;i<nbre_par;i++){
        trjk=new double[nim];
	for(jk=0;jk<nim;jk++)
 	    trjk[jk] = 100*jkn[jk][i+1]/par_init[i];

        PlotSerie ps= new  PlotSerie("Years",etiq,ty[i],trjk);
        ps.setFigure(3);
        Global.jackknifePlot[i]=new PlotHisto();
        Global.jackknifePlot[i].setValeurs(ps100);
        Global.jackknifePlot[i].setValeurs(ps);
 //Global.jackknifePlot[i].setValeurs(ps100);
        Global.jackknifePlot[i].setTitreGraphique(Global.titreG[i+17]);
        Global.jackknifePlot[i].setTitreX(Global.titreSx[i+17]);
        Global.jackknifePlot[i].setTitreY(Global.titreSy[i+17]);


        double maxY=Global.jackknifePlot[i].getMaxAxeY();
        if(maxY<150)
        {
           Global.jackknifePlot[i].setMinAxeY(0);
           Global.jackknifePlot[i].setMaxAxeY(maxY+10-(maxY%10));
           Global.jackknifePlot[i].setpasY(10.d);
        }
        else
         Global.jackknifePlot[i].ajusteExtremas(false,true,false,true);

        Global.jackknifePlot[i].setEtiquettes(etiq$);
        Global.jackknifePlot[i].setpasX(2.d);
        Global.jackknifePlot[i].setXcutYat(0.d);

}
trjk=new double[nim];
for(int i=0;i<nim;i++)
    trjk[i] = 10000*jkn[i][0]/r_tot;
        DecimalFormat nf = new DecimalFormat("0.00");
        PlotSerie ps= new  PlotSerie("Years",etiq,"",trjk);
        ps.setFigure(3);
        ps.setCouleur(Color.blue);
        Global.jackknifePlot[nbre_par]=new PlotHisto();
        Global.jackknifePlot[nbre_par].setValeurs(ps);
        Global.jackknifePlot[nbre_par].setValeurs(ps100);
        Global.jackknifePlot[nbre_par].setTitreGraphique(Global.titreG[21]+" (Jackknife R2 = "+nf.format(Global.jackknife)+")");
        Global.jackknifePlot[nbre_par].setTitreX(Global.titreSx[21]);
        Global.jackknifePlot[nbre_par].setTitreY(Global.titreSy[21]);

        double maxY=Global.jackknifePlot[nbre_par].getMaxAxeY();
        if(maxY<150)
        {
           Global.jackknifePlot[nbre_par].setMinAxeY(0);
           Global.jackknifePlot[nbre_par].setMaxAxeY(maxY+10-(maxY%10));
           Global.jackknifePlot[nbre_par].setpasY(10.d);
        }
        else
           Global.jackknifePlot[nbre_par].ajusteExtremas(false,true,false,true);

        Global.jackknifePlot[nbre_par].setEtiquettes(etiq$);
        Global.jackknifePlot[nbre_par].setpasX(2.d);
        Global.jackknifePlot[nbre_par].setXcutYat(0.d);


/*****************************MSE & MS-Y Plots***********************/
Global.msyPlot[0]=new Plot();
Global.msyPlot[1]=new Plot();
if(((Global.numero_modele > 5) || (Global.numero_modele < 2)) && (Global.numero_modele != 20)){
  double vv,ecartv;

  //System.out.println("num mod  " +Global.numero_modele);
  ecartv = (val_v[3] - val_v[2])/nv;
  vv = val_v[2];
  for(int iv=0;iv<nv;iv++){
	f_ms_m[iv] /= nim;
        //System.out.println("MSE : "+f_ms_m[iv]);
	f_ms_s[iv] /= nim;
	f_ms_s[iv] -= (f_ms_m[iv]*f_ms_m[iv]);
	if(f_ms_s[iv]>0.0)f_ms_s[iv] = Math.sqrt(f_ms_s[iv]);
		else f_ms_s[iv] = 0.0;
	vec_f_v[iv] = vv;
        //System.out.println("X : "+vv);
	vec_f_min[iv] = f_ms_m[iv] - 2.00 * f_ms_s[iv];
        //System.out.println("MSEmin : "+vec_f_min[iv]);
	vec_f_max[iv] = f_ms_m[iv] + 2.00 * f_ms_s[iv];
        //System.out.println("MSEmax : "+vec_f_max[iv]);
	y_ms_m[iv] /= nim;
	y_ms_s[iv] /= nim;
	y_ms_s[iv] -= (y_ms_m[iv]*y_ms_m[iv]);
	if(y_ms_s[iv]>0.0)y_ms_s[iv] = Math.sqrt(y_ms_s[iv]);
		else y_ms_s[iv] = 0.0;
	vec_y_v[iv] = vv;
	vec_y_min[iv] = y_ms_m[iv] - 2.00 * y_ms_s[iv];
	vec_y_max[iv] = y_ms_m[iv] + 2.00 * y_ms_s[iv];
	vv += ecartv;
  }

  PlotSerie[] pms=new PlotSerie[6];
  pms[0]=new PlotSerie("Environment (V)",vec_f_v,"MS_E",f_ms_m);
  pms[1]=new PlotSerie("Environment (V)",vec_f_v,"vec_f_min",vec_f_min);
  pms[2]=new PlotSerie("Environment (V)",vec_f_v,"vec_f_max",vec_f_max);

  pms[3]=new PlotSerie("Environment (V)",vec_y_v,"MS_Y",y_ms_m);
  pms[4]=new PlotSerie("Environment (V)",vec_y_v,"vec_y_min",vec_y_min);
  pms[5]=new PlotSerie("Environment (V)",vec_y_v,"vec_y_max",vec_y_max);

  for(int i=0;i<6;i++)
  {
      pms[i].setFigure(2);
      if(i==2 || i==5)
          pms[i].setCouleur(Color.yellow);
      else if(i==1 || i==4)
          pms[i].setCouleur(Color.green);
  }

  Global.msyPlot[0].setValeurs(pms[0]);
  Global.msyPlot[0].setValeurs(pms[1]);
  Global.msyPlot[0].setValeurs(pms[2]);
  Global.msyPlot[1].setValeurs(pms[3]);
  Global.msyPlot[1].setValeurs(pms[4]);
  Global.msyPlot[1].setValeurs(pms[5]);

  Global.msyPlot[0].setTitreGraphique("MS-E versus V");
  Global.msyPlot[1].setTitreGraphique("MSY versus V");
  for(int i=0;i<2;i++)
  {
    Global.msyPlot[i].setMinAxeY(0d);
    Global.msyPlot[i].setDecimalsurX(0);
    Global.msyPlot[i].ajusteExtremas(false,true,false,true);
    Global.msyPlot[i].ShowLegend=true;
  }
}


}

/*private static void saveResult(){
 String [] param$={"a","b","c","d"};
 try
 {
 PrintWriter out=new PrintWriter(new FileOutputStream("stat.txt",true));
 out.println( "Validation R² " +  Global.coeff_determination+ " Jacknife " +  Global.jackknife);
 out.println("-----------------------------------------------------");
 out.close();
 }
 catch(IOException ieo)
 {}

}*/

/*
  Donne les valeurs de MS_E pour les valeurs remarquables(moyenne-m�diane-min-max)
  @return un Object ou null si validation non Ok
*/

public static Object[][]  getMS_EResult(){
if(!Global.validationOk) return null;
Object[][] data$=new String[5][4];
String[] title$={"Remarquables values","MS_E Lower limit 95%","MS_E  Central Value ","MS_E Upper limit 95%"};
//String[] limitMS_V={"MS_V Lower limit 95%","MS_V  Central Value ","MS_V Upper limit 95%"};
String[] stat$={"   Mean: "," Median: ","Minimum: ","Maximum: "};
DecimalFormat nf= new DecimalFormat(" 0.00;-0.00");

for(int i=0;i<4;i++)
  stat$[i]=stat$[i]+ nf.format(val_v[i]);
for(int j=0;j<4;j++)
    data$[0][j]=title$[j];

for(int i=0;i<4;i++)
{
    data$[i+1][0]=stat$[i];
    data$[i+1][1]=nf.format(vec_f_min2[i]);
    data$[i+1][2]=nf.format(f_ms_m2[i]);
    data$[i+1][3]=nf.format(vec_f_max2[i]);
}

return data$;
}


/*
  Donne les valeurs de MS_Y pour les valeurs remarquables(moyenne-m�diane-min-max)
  @return un Object ou null si validation non Ok
*/
public static Object[][]  getMS_YResult(){
if(!Global.validationOk) return null;
Object[][] data$=new String[5][4];
String[] title$={"Remarquables values","MS_Y Lower limit 95%","MS_Y  Central Value ","MS_Y Upper limit 95%"};
String[] stat$={"   Mean: "," Median: ","Minimum: ","Maximum: "};
DecimalFormat nf= new DecimalFormat(" 0.00;-0.00");

for(int i=0;i<4;i++)
  stat$[i]=stat$[i]+ nf.format(val_v[i]);

for(int j=0;j<4;j++)
    data$[0][j]=title$[j];

for(int i=0;i<4;i++)
{
    data$[i+1][0]=stat$[i];
    data$[i+1][1]=nf.format(vec_y_min2[i]);
    data$[i+1][2]=nf.format(y_ms_m2[i]);
    data$[i+1][3]=nf.format(vec_y_max2[i]);
}

return data$;
}

/*
  Donne le d�tails des r�sultats sur les param�tres du mod�le suite � la validation
  @return un tableau Object ou null si validation non Ok
*/
public static Object[][]  getParamResult(){

if(!Global.validationOk) return null;

String [] param$={"a","b","c","d"};
String [] title$={"Parameters","Value","Jackknife Standard deviation","Jackknife t-ratio"};
Object[][] data$=new String[nbre_par+6][4];
DecimalFormat nf= new DecimalFormat(" 0.000000;-0.000000");
DecimalFormat nf2= new DecimalFormat(" 0.00");

     for(int j=0;j<4;j++)
          data$[0][j]=title$[j];
     for(int i=0;i<nbre_par;i++)
     {
          data$[i+1][0]=param$[i];
          data$[i+1][1]=nf.format(par_init[i]);
          data$[i+1][2]=nf.format(ecart_pseudo[i]);
         // System.out.println("i" + i + " "+ ecart_pseudo[i]);
          if(ecart_pseudo[i]!=0)
            data$[i+1][3]=nf.format(par_init[i]/ecart_pseudo[i]);
          else
            data$[i+1][3]="na";
     }
     data$[nbre_par+1][0]="Conventional R²";
     data$[nbre_par+1][1]=nf2.format(Global.coeff_determination);
     data$[nbre_par+2][0]= "Jackknife  R²";
     data$[nbre_par+2][1]=nf2.format(Global.jackknife);
     data$[nbre_par+3][0]= "T_Jackknife";
     if(Global.test_jackknife)
          data$[nbre_par+3][1]="good";
     else
          data$[nbre_par+3][1]="bad";
     System.out.println("nbpar : "+nbre_par);
     data$[nbre_par+4][0]= "Fisher test of R² for F(" + getNbDataRetenue() + "," + Global.nbre_param +")";
     data$[nbre_par+4][1]=Double.toString(Global.pfisher(Global.coeff_determination, getNbDataRetenue(), Global.nbre_param));
     data$[nbre_par+5][0]= "Fisher threshold";
     data$[nbre_par+5][1]=nf.format(Global.fF(Global.coeff_determination, getNbDataRetenue(), Global.nbre_param));
     for(int i=1;i<4;i++)
     {
          data$[nbre_par+i][2]="";
          data$[nbre_par+i][3]="";
     }
return data$;
}

/*
  Donne la valeur des param�tres pour chaque ann�e suite � la validation
  @return un tableau Object ou null si validation non Ok
*/
public static Object[][]  getYearResult(){

if(!Global.validationOk) return null;

String[] title$={"Years","a (%)","b (%)","c (%)","d (%)"};
double[] years=Data.getYears();
Object[][] data$=new String[nim+1][nbre_par+2];
DecimalFormat nf0= new DecimalFormat("0");
DecimalFormat nf= new DecimalFormat(" 0.00;-0.00");
for(int j=0;j<nbre_par+1;j++)
          data$[0][j]=title$[j];
data$[0][nbre_par+1]="R²";

for(jk=0;jk<nim;jk++)
 	    data$[jk+1][0] = nf0.format(years[jk]);

for(int i=0;i<nbre_par;i++){
        for(jk=0;jk<nim;jk++)
 	    data$[jk+1][i+1] = nf.format(100*jkn[jk][i+1]/par_init[i]);
}
for(int i=0;i<nim;i++)
    data$[i+1][nbre_par+1] = nf.format(10000*jkn[i][0]/r_tot);



return data$;
}



}