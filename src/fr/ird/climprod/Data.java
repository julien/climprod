/**
 * Titre : Climprod<p>
 *
 */
package fr.ird.climprod;

import java.util.StringTokenizer;
import java.text.*;//DecimalFormat;

public class Data {

    private static String fileName = null;
    private static int nbData = 0;
    private static double[][] data;
    private static boolean existFile = false;

    private static double[] fcoeff = new double[13];
    private static double[] vcoeff = new double[13];
    private static double[] year;
    private static double[] yexp;
    private static double[] pue;
    private static double[] f;
    private static double[] v;
    private static double[] vbar;
    private static int decalmax = 0;
    private static double[][] stat = new double[10][4];
    private static Object[][] dataTable;
    private static Object[][] dataCorrelation;

    public static void readFile(String file) {
        dataTable = null;
        dataCorrelation = null;
        fileName = file;
        existFile = false;
        try {
            String[] dataLine;
            String[] fieldNames = {"Years", "Production", "Effort", "Environment"};
            ReadFileText rft = new ReadFileText(file);
            dataLine = rft.getLines();
            nbData = dataLine.length - 1;
            data = new double[nbData][4];
            if (dataLine != null) //Lecture ok
            {
                StringTokenizer t = new StringTokenizer(dataLine[0], ";");
                if (t.countTokens() != 4) {
                    throw new OnError("Invalid data file");
                }
                for (int i = 0; i < 4; i++) {
                    if ((t.nextToken().trim()).equals(fieldNames[i]) == false) {
                        throw new OnError("Invalid data file.");
                    }
                }
                for (int i = 1; i < dataLine.length; i++) {
                    StringTokenizer d = new StringTokenizer(dataLine[i], ";");
                    if (d.countTokens() != 4) {
                        throw new OnError("Invalid data file");
                    }
                    int k = 0;
                    while (d.hasMoreTokens()) {
                        data[i - 1][k] = Double.parseDouble(d.nextToken().trim());
                        //System.out.print(data[i-1][k]+"  ");
                        k = k + 1;
                    }
                    // System.out.println();
                }

            }
            existFile = true;
            init_val();
            makePlot(false);
            statistiques();
            makeDataTable();
            makeCorrelationTable();
        } catch (Exception e) {
            nbData = 0;
            MsgDialogBox msg = new MsgDialogBox(0, e.getMessage(), 0, Global.CadreMain);
        }
    }

    /**
     * Retourne le nombre de donn�es
     *
     * @return un entier repr�sentant le nombre d'ann�es de donn�es ou -1 si
     * fichier non valide
     */
    public static int getNbYears() {
        if (existFile) {
            return nbData;
        } else {
            return -1;
        }
    }

    /**
     * Retourne le nom complet du fichier
     *
     * @return un String repr�sentant le nom complet du fichier ou null si
     * fichier non valide
     */
    public static String getFileName() {
        if (existFile) {
            return fileName;
        } else {
            return null;
        }
    }

    /**
     * Retourne les donn�es d'origine
     *
     * @return un double[][] ou null si fichier non valide
     */
    public static double[][] getData() {
        if (existFile) {
            return (double[][]) data.clone();
        } else {
            return null;
        }
    }

    /**
     * Retourne la production yexp apr�s prise en cpte du d�calage
     *
     * @return un double[], la production ou null si fichier non valide
     */
    public static double[] getYexp() {
        if (existFile) {
            return yexp;
        } else {
            return null;
        }
    }

    /**
     * Retourne la production par unite d'effort de peche apr�s prise en cpte du
     * d�calage
     *
     * @return un double[], ou null si fichier non valide
     */
    public static double[] getPue() {
        if (existFile) {
            return pue;
        } else {
            return null;
        }
    }

    /**
     * Retourne l'environnement apr�s prise en cpte du d�calage
     *
     * @return un double[], ou null si fichier non valide
     */
    public static double[] getV() {
        if (existFile) {
            return v;
        } else {
            return null;
        }
    }

    /**
     * Retourne l'effort de peche apr�s prise en cpte du d�calage
     *
     * @return un double[], ou null si fichier non valide
     */
    public static double[] getF() {
        if (existFile) {
            return f;
        } else {
            return null;
        }
    }

    /**
     * Retourne l'effort de peche apr�s prise en cpte du d�calage
     *
     * @return un double[], ou null si fichier non valide
     */
    public static double[] getVbar() {
        if (existFile) {
            return vbar;
        } else {
            return null;
        }
    }

    /**
     * Retourne la periode d'observation apr�s prise en cpte du d�calage
     *
     * @return un double[], ou null si fichier non valide
     */
    public static double[] getYears() {
        if (existFile) {
            return year;
        } else {
            return null;
        }
    }

    /**
     * Retourne le nombre de donn�es retenues
     *
     * @return un entier, ou null si fichier non valide
     */
    public static int getNbDataRetenue() {
        if (existFile) {
            return nbData - decalmax;
        } else {
            return -1;
        }
    }

    public static void init_val() {
        int i, d;
        calcul_coeff();
        int nim = nbData - decalmax;
        // System.out.println("nim  " + nim);
        year = new double[nim];
        yexp = new double[nim];
        pue = new double[nim];
        f = new double[nim];
        v = new double[nim];
        vbar = new double[nim];
        for (i = 0; i < nim; i++) {
            year[i] = data[i + decalmax][0];
            yexp[i] = data[i + decalmax][1];
            v[i] = data[i + decalmax][3];
            f[i] = 0;
            vbar[i] = 0;
            for (d = 0; d <= decalmax; d++) {
                f[i] += data[i + decalmax - d][2] * fcoeff[d];
                vbar[i] += data[i + decalmax - d][3] * vcoeff[d];
            }
            pue[i] = yexp[i] / data[i + decalmax][2];
            //System.out.println(data[i+decalmax][0]+"*"+yexp[i]+"*"+v[i]+"*"+f[i]+"*"+vbar[i]+"*"+ pue[i]);
        }
        makePlot(true);

    }

    /**
     *
     */
    private static void calcul_coeff() {
        int nbexploit = Global.nb_classes_exploitees;
        int agerec = Global.recruitment_age;
        int begining = Global.begin_influence_period;
        int ending = Global.end_influence_period;
       //System.out.println(nbexploit+"****"+agerec+ "**********" +begining+"******"+ending);

        /*int nbexploit=Integer.parseInt((String)Global.htVar.get("nb_classes_exploitees"));
         int agerec=Integer.parseInt((String)Global.htVar.get("recruitment_age"));
         int begining=Integer.parseInt((String)Global.htVar.get("begin_influence_period"));
         int ending=Integer.parseInt((String)Global.htVar.get("end_influence_period"));*/
        if (nbexploit < 1) {
            nbexploit = 1;
        }
        if (agerec < 1) {
            agerec = 0;
        }
        if (agerec > 10) {
            agerec = 10;
        }
        if (ending < begining) {
            begining = ending;
        }

        double[][] t = new double[16][21];
        int ageder;
        double eps = 1.0E-12;
        double dtot;
        for (int d = 0; d < 13; d++) {
            fcoeff[d] = 0;
        }
        for (int d = 0; d < 13; d++) {
            vcoeff[d] = 0;
        }
        dtot = 0;
        for (int d = 0; d < nbexploit; d++) {
            dtot += (nbexploit - d);
        }
        for (int d = 0; d < nbexploit; d++) {
            fcoeff[d] = (nbexploit - d) / dtot;
        }
        decalmax = nbexploit - 1;
        for (int i = 0; i < 15; i++) {
            for (int j = 0; j < 21; j++) {
                t[i][j] = 0;
            }
        }
        ageder = agerec + nbexploit - 1;
        dtot = 0;
       //System.out.println(nbexploit+"****"+agerec+ "**********" +begining+"******"+ending+"********"+ageder);
        //System.out.println(decalmax);
        for (int i = 0; i < nbexploit; i++) {
            for (int d = begining; d <= ending; d++) {
                t[i][d + i] = 1;
                //System.out.println(i+" " +(d+i-1) );
            }
        }
        for (int d = 0; d < ageder + 1; d++) {
            for (int i = 0; i < nbexploit; i++) {
                vcoeff[ageder - d] += t[i][d];
                // System.out.println(ageder-d+"***"+vcoeff[d]);
                dtot += t[i][d];
                //System.out.println("dtot "+i+" " +d +" " +dtot);
            }
        }
        for (int d = 0; d < ageder + 1; d++) {
            vcoeff[d] /= dtot;
        }
        //for(int d=0;d<=ageder;d++)
        //System.out.println(d+"----"+vcoeff[d]);
        int d = ageder;
        //System.out.println(d+"****ll***"+vcoeff[d]+"*******"+decalmax);
        while (vcoeff[d] < eps) {
            d--;
        }
        if (d > decalmax) {
            decalmax = d;
        }
        //System.out.println(decalmax);
        //decalmax=1;
        //if(Global.numero_modele==-2)
        //      decalmax=0;

    }

    private static void makePlot(boolean lag) {
        if (lag) {
            /* ************** Lag plot ******************************/
            for (int i = 0; i < 2; i++) {
                Global.lagPlot[i] = new Plot();
            }
            Global.lagPlot[0].setValeurs(new PlotSerie(" ", Data.f, " ", Data.pue));
            Global.lagPlot[1].setValeurs(new PlotSerie(" ", Data.v, " ", Data.pue));

            for (int i = 0; i < 2; i++) {
                Global.lagPlot[i].setTitreGraphique(Global.titreG[i + 10]);
                Global.lagPlot[i].setTitreX("weighted " + Global.titreSx[i + 10]);
                Global.lagPlot[i].setTitreY(Global.titreSy[i + 10]);
                double max = Math.abs(Global.lagPlot[i].getMaxAxeX() - Global.lagPlot[i].getMinAxeX());
                if (max > 10) {
                    Global.lagPlot[i].setDecimalsurX(0);
                }
                max = Math.abs(Global.lagPlot[i].getMaxAxeY() - Global.lagPlot[i].getMinAxeY());
                if (max > 10) {
                    Global.lagPlot[i].setDecimalsurY(0);
                }
            }
        } else {
            /**
             * ***************** time plot *****************************
             */
            double minx = Stat1.min(Data.year);
            PlotSerie[] ps = new PlotSerie[4];
            ps[2] = new PlotSerie("x", Data.year, "y", Data.yexp);
            ps[3] = new PlotSerie("x", Data.year, "y", Data.pue);
            ps[0] = new PlotSerie("x", Data.year, "y", Data.f);
            ps[1] = new PlotSerie("x", Data.year, "y", Data.v);
            for (int i = 0; i < 4; i++) {
                Global.timePlot[i] = new Plot();
                ps[i].setFigure(2);
                Global.timePlot[i].setValeurs(ps[i]);
                Global.timePlot[i].setTitreGraphique(Global.titreG[i]);
                Global.timePlot[i].setTitreX(Global.titreSx[i]);
                Global.timePlot[i].setTitreY(Global.titreSy[i]);
                Global.timePlot[i].setDecimalsurX(0);
                Global.timePlot[i].setMinAxeX(minx - 1);
                Global.timePlot[i].setpasX(2.0d);
                double max = Math.abs(Global.timePlot[i].getMaxAxeY() - Global.timePlot[i].getMinAxeY());
                if (max > 10) {
                    Global.timePlot[i].setDecimalsurY(0);
                }
                //Global.timePlot[i].ShowLegend=true;
            }

            /**
             * **************** Distribution plot ********************************
             */
            for (int i = 0; i < 4; i++) {
                double[][] zz = new double[10][2];
                double[] val = new double[10];
                double[] etiq = new double[10];
                String[] etiq$ = new String[10];
                for (int k = 0; k < 10; k++) {
                    etiq[k] = k + 1;
                }
                Global.distriPlot[i] = new PlotHisto();
                switch (i) {
                    case 0:
                        zz = Stat1.distribuer(10, Data.f);
                        break;
                    case 1:
                        zz = Stat1.distribuer(10, Data.v);
                        break;
                    case 2:
                        zz = Stat1.distribuer(10, Data.yexp);
                        break;
                    case 3:
                        zz = Stat1.distribuer(10, Data.pue);
                        break;

                }
                DecimalFormat df = new DecimalFormat("0.##");
                if (zz[0][0] >= 100) {
                    df = new DecimalFormat("0");
                }
                for (int j = 0; j < zz.length; j++) {
                    etiq$[j] = df.format(zz[j][0]).toString();
                    val[j] = zz[j][1];
                }
                PlotSerie psd = new PlotSerie("x", etiq, "y", val);
                psd.setEtiquettes(etiq$);
                psd.setFigure(3);
                Global.distriPlot[i].setValeurs(psd);
                Global.distriPlot[i].setTitreGraphique(Global.titreG[i + 4]);
                Global.distriPlot[i].setTitreX(Global.titreSx[i + 4]);
                Global.distriPlot[i].setTitreY(Global.titreSy[i + 4]);
                Global.distriPlot[i].setDecimalsurY(0);
                Global.distriPlot[i].setpasY(1.d);
                Global.distriPlot[i].setMaxAxeY(psd.getMaxY());
            }

            /* ************** Scatter plot ******************************/
            for (int i = 0; i < 4; i++) {
                Global.scatterPlot[i] = new Plot();
            }

            Global.scatterPlot[0].setValeurs(new PlotSerie(" ", Data.f, " ", Data.yexp));
            Global.scatterPlot[1].setValeurs(new PlotSerie(" ", Data.v, " ", Data.yexp));
            Global.scatterPlot[2].setValeurs(new PlotSerie(" ", Data.f, " ", Data.pue));
            Global.scatterPlot[3].setValeurs(new PlotSerie(" ", Data.v, " ", Data.pue));

            for (int i = 0; i < 4; i++) {
                Global.scatterPlot[i].setTitreGraphique(Global.titreG[i + 8]);
                Global.scatterPlot[i].setTitreX(Global.titreSx[i + 8]);
                Global.scatterPlot[i].setTitreY(Global.titreSy[i + 8]);
                double max = Math.abs(Global.scatterPlot[i].getMaxAxeX() - Global.scatterPlot[i].getMinAxeX());
                if (max > 10) {
                    Global.scatterPlot[i].setDecimalsurX(0);
                }
                max = Math.abs(Global.scatterPlot[i].getMaxAxeY() - Global.scatterPlot[i].getMinAxeY());
                if (max > 10) {
                    Global.scatterPlot[i].setDecimalsurY(0);
                }

            }

            /* ************** Ind�pendance plot *************************/
            Global.indePlot = new Plot();
            Global.indePlot.setValeurs(new PlotSerie("Environement (V)", Data.v, "Effort (E)", Data.f));
            Global.indePlot.setTitreGraphique(Global.titreG[12]);

        }

    }

    /*
     *Calcule les principales statistiques sur les donn�es.
     *Utilis� � la lecture du fichier (� ce stade la variable stat ne tient past cpte du d�calage)
     */
    private static void statistiques() {

        double[][] trans = new double[4][];
        trans[0] = yexp;
        trans[2] = f;
        trans[3] = v;
        trans[1] = pue;
        for (int j = 0; j < 4; j++) {
            stat[0][j] = nbData;
            stat[1][j] = Stat1.moy(trans[j]);
            stat[2][j] = Math.sqrt(Stat1.var(trans[j], true));
            stat[3][j] = 100 * stat[2][j] / (stat[1][j] + 1.e-20);
            stat[4][j] = Stat1.Kurtosis(trans[j]);
            stat[5][j] = Stat1.Skewness(trans[j]);
            double[] ext = Stat1.Extremas(trans[j]);
            stat[6][j] = ext[0];
            stat[7][j] = ext[1];
            stat[8][j] = ext[1] - ext[0];
            ext = Stat1.quant(trans[j], 3);
            stat[9][j] = ext[1];
        }

    }

    /*
     Donne les principaux r�sultats
     @return Object[][] ou null si fichier invalide
     */
    public static Object[][] getStatistics() {
        if (!existFile) {
            return null;
        }
        String[] param$ = {"Cases",
            "Mean",
            "Stand-dev",
            "Coef.Var",
            "Kurtosis",
            "Skewness",
            "Minimum",
            "Maximum",
            "Range",
            "Median"
        };
        String[] title$ = {"Statistics  ", "Production", "C.P.U.E", "Effort", "Environment"};
        Object[][] data$ = new String[11][5];
    // DecimalFormat nf0= new DecimalFormat("0");
        //  DecimalFormat nf= new DecimalFormat(" 0.000000;-0.000000");
        DecimalFormat nf = new DecimalFormat(" 0.######;-0.######");
        for (int j = 0; j < 5; j++) {
            data$[0][j] = title$[j];
        }

        for (int i = 0; i < 10; i++) {
            data$[i + 1][0] = param$[i];

            for (int j = 0; j < 4; j++) {
                data$[i + 1][j + 1] = nf.format(stat[i][j]);
            }
            //nf0=nf;
        }
        return data$;
    }


    /*
     Donne le tableau de donn�es (attention ne prend pas en cpte le d�calage)
     @return Object[][] ou null si fichier invalide
     */
    public static Object[][] getDataTable() {

        if (!existFile) {
            return null;
        }

        return dataTable;
    }

    /*
     Donne le tableau de correlation entre variables
     @return Object[][] ou null si fichier invalide
     */
    public static Object[][] getCorrelationTable() {

        if (!existFile) {
            return null;
        }
        return dataCorrelation;

    }

    /*
     *Donne le tableau de donn�es
     *Utilis� � la lecture du fichier (� ce stade la variable stat ne tient past cpte du d�calage)
     */
    private static void makeDataTable() {

        if (!existFile) {
            return;
        }
        String[] title$ = {"Years  ", "Production", "C.P.U.E", "Effort", "Environment"};
        dataTable = new String[nbData + 1][5];
    // DecimalFormat nf0= new DecimalFormat(" 0;-0");
        // DecimalFormat nf= new DecimalFormat(" 0.000000;-0.000000");
        DecimalFormat nf = new DecimalFormat(" 0.######;-0.######");
        for (int j = 0; j < 5; j++) {
            dataTable[0][j] = title$[j];
        }
        for (int i = 0; i < nbData; i++) {
            dataTable[i + 1][0] = nf.format(year[i]);
            dataTable[i + 1][1] = nf.format(yexp[i]);
            dataTable[i + 1][2] = nf.format(pue[i]);
            dataTable[i + 1][3] = nf.format(f[i]);
            dataTable[i + 1][4] = nf.format(v[i]);

        }

    }


    /*
     Donne le tableau de donn�es (attention prend en cpte le d�calage)
     @return Object[][] ou null si fichier invalide
     */
    private static void makeCorrelationTable() {

        if (!existFile) {
            return;
        }
        String[] title$ = {"Correlations", "Production", "C.P.U.E", "Effort", "Environment"};
        dataCorrelation = new String[5][5];
        DecimalFormat nf = new DecimalFormat(" 0.000000;-0.000000");

        for (int i = 1; i < 5; i++) {
            for (int j = 1; j < 5; j++) {
                if (i == j) {
                    dataCorrelation[i][j] = nf.format(1);
                } else {
                    dataCorrelation[i][j] = "";
                }
            }
        }
        for (int i = 1; i < 5; i++) {
            dataCorrelation[i][0] = title$[i];
        }
        for (int j = 0; j < 5; j++) {
            dataCorrelation[0][j] = title$[j];
        }
        dataCorrelation[2][1] = nf.format(Stat1.rho(yexp, pue));
        dataCorrelation[3][1] = nf.format(Stat1.rho(f, yexp));
        dataCorrelation[3][2] = nf.format(Stat1.rho(f, pue));
        dataCorrelation[4][1] = nf.format(Stat1.rho(v, yexp));
        dataCorrelation[4][2] = nf.format(Stat1.rho(v, pue));
        dataCorrelation[4][3] = nf.format(Stat1.rho(v, f));

    }

}
