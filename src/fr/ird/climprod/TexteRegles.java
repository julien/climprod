
/**
 * Titre : Climprod<p>
 */
package fr.ird.climprod;
import java.io.InputStream;
import java.util.StringTokenizer;
import java.util.Hashtable;
import java.util.HashMap;
import java.text.DecimalFormat;
public class TexteRegles {

    private static String [] script;
    private static String [][] item;
    private static HashMap<String, String> htNum=new HashMap<String, String>();
    private static HashMap<String, String> htComment=new HashMap<String, String>();
    private static boolean[] lethal;
    private static boolean[] posee;
    private static int numEnCours;
    private static int indexEnCours;
    private static String scriptEnCours;
    private static String[] itemEnCours;
    private static String commentaireEnCours="";
    private static boolean R2limite=false;

  //public static void initScript(String file,String fileComment)
  public static void initScript(InputStream file,InputStream fileComment)
  {
      try
      {
        int nbData=0;
        String[] dataLine;

        ReadFileText rft=new ReadFileText(file);
        dataLine=rft.getLines();
        nbData=dataLine.length;
        script=new String[nbData];
        item=new String[nbData][];
        lethal=new boolean[nbData];
        posee=new boolean[nbData];

        initRegles();
        for (int i=0;i<dataLine.length;i++)
        {
             StringTokenizer d=new StringTokenizer (dataLine[i],";");
             htNum.put(d.nextToken().trim(),Integer.toString(i));
             if(d.nextToken().trim().equals("0"))
                lethal[i]=false;
             else
                lethal[i]=true;
             script[i]=d.nextToken().trim();
             int k=d.countTokens();
             if (k!=0)
             {
                item[i]=new String[k];
                for (int j=0;j<k;j++)
                      item[i][j]=d.nextToken().trim();
             }

           /* System.out.println(script[i]);
             for(int l=0;l<k;l++)
               System.out.print(item[i][l]+"  ");
             System.out.println("******************");*/
        }
        rft=new ReadFileText(fileComment);
        dataLine=rft.getLines();
        for (int i=0;i<dataLine.length;i++)
        {
             String c$="";
             String n$;
             StringTokenizer d=new StringTokenizer (dataLine[i],";");
             n$=d.nextToken().trim();
             while(d.hasMoreTokens())
                  c$=c$+d.nextToken().trim()+"\n";
             htComment.put(n$,c$);
             //htComment.put(d.nextToken().trim(),d.nextToken());
        }
      }
      catch(Exception e)
      {
         MsgDialogBox msg=new MsgDialogBox(0,e.getMessage(),0, Global.CadreMain);
      }
}

public static void loadRegles(int number)
{
    numEnCours=number;
    System.out.println("on charge la règle " +  numEnCours );
    commentaireEnCours="";
    if (numEnCours !=-1)
    {
         indexEnCours=Integer.parseInt((String)htNum.get(Integer.toString(numEnCours)));
         scriptEnCours=script[indexEnCours];
         itemEnCours=new String[item[indexEnCours].length];
         for (int i=0;i<itemEnCours.length;i++)
                itemEnCours[i]=item[indexEnCours][i];


    }
    else
    {
         scriptEnCours=null;
         itemEnCours=null;

    }

}

public static String getScript(){
    //if(posee[numEnCours]==true) return "";
    posee[indexEnCours]=true;
    return scriptEnCours;
}

public static String[] getItem(){

    return itemEnCours;
}

public static void initRegles()
{
      for(int i=0;i<posee.length;i++)
            posee[i]=false;

}

/*
    Teste la règle
    @return boolean true si règle vraie.
*/
public static boolean isTrue(){
          boolean result=false;
          commentaireEnCours="";
         DecimalFormat nf= new DecimalFormat(" 0.00");
          switch(numEnCours)
          {
              case 41:
                    result =  Data.getNbYears()<12;
                   // result=true;
                    if(!result)
                        commentaireEnCours="The number of years of observation of your data set is " +Data.getNbYears() ;
                    break;
              case 42:
                    result =  (Global.changement_exploitation!=2);
                    break;
              case 43:
                    result =  (Global.unite_standardisee!=1);
                    break;
              case 47:
                    double temp;
                    double[] effort=Data.getF();
                    double[] ext=Stat1.Extremas(effort);
                    temp=(ext[1]-ext[0]/ext[0]);
                    result =  temp<0.4d;
                    if(!result)
                        commentaireEnCours="The range of fishing effort variation of your data set is " + (temp);
                    break;
              case 44:
                    result =  (Global.effet_delais_abundance_negligeable!=1);
                    break;
              case 46:
                    result =  (Global.stock_unique!=1 && Global.sous_stock!=1);
                    break;
              case 45:
                    result =  (Global.stock_unique!=1 && Global.sous_stock==1 && Global.sous_stock_isole!=1);
                    break;
              case 34:
                    result =  (Global.stock_unique!=1 && Global.sous_stock==1 && Global.sous_stock_isole==1);
                    break;
              case 35:
                    result = (Global.under_and_over_exploited!=1  && Global.under_and_optimaly==1);
                    break;
              case 49:
                    result = (Global.under_and_over_exploited!=1 && Global.under_and_optimaly!=1);
                    break;
              case 37:
                    result =  (Global.statistiques_anormales==1);
                    break;
              case 36:
                    result =  (Global.unstability==1);
                    break;
              case 38:
                    result = (Global.abnormal_points_dist==1);
                    break;
              case 39:
                    result = (Global.abnormal_points_scatt==1);
                    break;
              case 50:
                    result = (Global.effort_increasing==1);
                    break;
              case 40:
                    result = (Global.independance==1);
                    break;
              case 48:
                    result = (Global.effort_preponderant==1 && Global.decreasing_relationship==2);
                    break;
              case 10:
                    //System.out.println(Global.relationCPU_E+" "+Global.pessimiste+" "+Global.stock_deja_effondre+" "+Global.lifespan+" "+Global.fecondite_faible);
                    result = (Global.relationCPU_E==RechercheModele.exponentiel && Global.pessimiste!=1 && Global.stock_deja_effondre!=1 && Global.lifespan>10 && Global.fecondite_faible!=1);
                    break;
              case 11:
                    result = (Global.relationCPU_E==RechercheModele.exponentiel && Global.pessimiste!=1 && Global.stock_deja_effondre!=1 && Global.lifespan>4 && Global.lifespan<11 && Global.fecondite_faible!=1 && Global.rapport_vie_exploitee_inferieur_deux==2);
                    break;
              case 12:
                    result = (Global.relationCPU_E==RechercheModele.exponentiel && Global.pessimiste!=1 && Global.stock_deja_effondre!=1 && Global.lifespan>4 && Global.lifespan<11 && Global.fecondite_faible!=1 && Global.premiere_reproduction_avant_recrutement==1);
                    break;
              case 13:
                    result = (Global.relationCPU_E==RechercheModele.exponentiel && Global.pessimiste!=1 && Global.stock_deja_effondre!=1 && Global.lifespan>4 && Global.lifespan<11 && Global.fecondite_faible!=1 && Global.reserves_naturelles==1);
                    break;
              case 14:
                    result = (Global.relationCPU_E==RechercheModele.exponentiel && Global.pessimiste!=1 && Global.stock_deja_effondre!=1 && Global.lifespan>4 && Global.lifespan<11 && Global.fecondite_faible!=1 && Global.stock_divise==1);
                    break;
              case 16:
                    result = (Global.relationCPU_E==RechercheModele.exponentiel && Global.pessimiste!=1 && Global.stock_deja_effondre!=1 && Global.rapport_vie_exploitee_inferieur_deux==2 && Global.premiere_reproduction_avant_recrutement==1 && Global.fecondite_faible!=1 && Global.reserves_naturelles==1 && Global.stock_divise==1);
                    break;
              case 17:
                    result = (Global.relationCPU_E==RechercheModele.lineaire);
                    break;
              case 18:
                    result = (Global.pessimiste==1);
                    if(result)
                        Global.relationCPU_E=RechercheModele.lineaire;
                    else
                         Global.relationCPU_E=RechercheModele.exponentiel;
                    break;
              case 19:
                    result = (Global.stock_deja_effondre==1);
                    if(result)
                        Global.relationCPU_E=RechercheModele.lineaire;
                    else
                         Global.relationCPU_E=RechercheModele.exponentiel;
                    break;
              case 20:
                    result = (Global.lifespan<5 && Global.rapport_vie_exploitee_inferieur_deux==1 && Global.premiere_reproduction_avant_recrutement==2);
                    if(result)
                        Global.relationCPU_E=RechercheModele.lineaire;
                    else
                         Global.relationCPU_E=RechercheModele.exponentiel;
                    break;
              case 21:
                    result = (Global.fecondite_faible==1);
                    if(result)
                        Global.relationCPU_E=RechercheModele.lineaire;
                    else
                         Global.relationCPU_E=RechercheModele.exponentiel;
                    break;
              case 22:
                    result = (Global.lifespan<5 && Global.rapport_vie_exploitee_inferieur_deux==1 && Global.reserves_naturelles==2 && Global.stock_divise==2 && Global.cpue_unstable==2);
                    if(result)
                        Global.relationCPU_E=RechercheModele.lineaire;
                    else
                         Global.relationCPU_E=RechercheModele.exponentiel;
                    break;
              case 23:
                    Global.relationCPU_E=RechercheModele.general; //Tjrs vrai
                    result=true;
                    break;
              case 24:
                   // Global.coeff_determination=0.3;  //test general
                    result=(Global.relationCPU_E==RechercheModele.lineaire && Global.coeff_determination<0.4);
                    if(result)
                    {
                         Global.relationCPU_E=RechercheModele.general;
                         commentaireEnCours="The coefficient of determination of the model " + RechercheModele.getEquation()+ " is " + Global.coeff_determination+"\n";
                         commentaireEnCours=commentaireEnCours+"A generalized model is going to be fitted";
                    }
                    break;
              case 25:
                    result=(Global.relationCPU_E==RechercheModele.exponentiel && Global.coeff_determination<0.4);
                    if(result) Global.relationCPU_E=RechercheModele.general;
                    break;
              case 26:
                    result = (Global.linear_relationship==1);
                    break;
              case 27:
              case 28:
                    result = (Global.linear_relationship==2 && Global.monotonic_relationship==1);
                    break;
              case 29:
                    result = (Global.linear_relationship==2 && Global.monotonic_relationship==2);
                    break;
              case 51:
                    //Global.val_param[2]=2;
                    result = (Global.relationCPU_E==RechercheModele.general && Global.val_param[2]<2.2d && Global.val_param[2]>1.8d);
                    if (result)
                    {
                        Global.relationCPU_E=RechercheModele.lineaire;
                        R2limite=true;

                    }
                    commentaireEnCours="Coefficient c (m in the Pella and Tomlinson notation) value is :" + Global.val_param[2];
                    break;
              case 52:
                    result = (Global.relationCPU_E==RechercheModele.general && Global.pessimiste!=1 && Global.val_param[2]<1.2d && Global.val_param[2]>0.8d);
                    if (result)
                    {
                        Global.relationCPU_E=RechercheModele.exponentiel;
                        R2limite=true;
                    }
                    commentaireEnCours="Coefficient c (m in the Pella and Tomlinson notation) value is :" + Global.val_param[2];
                    break;
              case 53:
                    result=(Global.coeff_determination<0.4);
                    if(result)
                    {
                      commentaireEnCours="The coefficient of determination of the model " + RechercheModele.getEquation()+ " is " +nf.format(Global.coeff_determination)+"\n";
                      commentaireEnCours=commentaireEnCours+ "From the current rule this model is not convenient.";
                     }
                    break;
              case 54:
                    //Global.coeff_determination=0.3;
                    result=(Global.coeff_determination<0.4);
                    if(result)
                    {
                      commentaireEnCours="The coefficient of determination of the model " + RechercheModele.getEquation()+ " is " +nf.format(Global.coeff_determination)+"\n";
                      commentaireEnCours=commentaireEnCours+ "From the current rule this model is not convenient.";
                      commentaireEnCours=commentaireEnCours+"\n There is no available model appropriate to your case";
                     }
                    //result=(Global.typeModele ==  RechercheModele.mixte)  ;
                    break;
                   // commentaireEnCours="The value of coefficient of determination is " + Global.coeff_determination;
                  //  if(result) commentaireEnCours=commentaireEnCours+"/n There is no available model appropriate to your case";
              case 55:
                  result = (Global.relationCPU_E==RechercheModele.general);
                  //Global.test_jackknife = true;
                    //if(result)
                    //    commentaireEnCours="Oui le modèle est généralisé";
                    break;
              case 56:
                  result = true;
                  if (Global.CadreJackniffePlots == null) {
                      Cadre_Plot dlg;
                      dlg = new Cadre_Plot(Global.jackknifePlot);
                      dlg.setTitle("Climprod : Jackknife plots");
                      dlg.setVisible(true);
                      Global.CadreJackniffePlots = dlg;
                  } else {
                      Global.CadreJackniffePlots.setVisible(true);
                  }
                  break;
              case 57:
                  result = false;
                  break;
              case 4:
                    result = (Global.effort_preponderant==1 && Global.cpue_sous_sur_production!=2 && Global.coeff_determination>0.7);
                    System.out.println("eff :"+Global.effort_preponderant+" sous sur : "+Global.cpue_sous_sur_production+" coef det : "+Global.coeff_determination);
                    if(result)
                        commentaireEnCours="From the current rule the model " + RechercheModele.getEquation()+ " is convenient.";
                    break;
              case 5:
                    result = (Global.effort_preponderant==1 && Global.cpue_sous_sur_production==1 && Global.coeff_determination>0.7);
                    if(result)
                        commentaireEnCours="From the current rule the model " + RechercheModele.getEquation()+ " is convenient.";
                    else
                        commentaireEnCours="Model is not convenient because R2 = "+ Global.coeff_determination + " .\n\nYou may still generate a report.";
                    break;
              case 6:
                    result = (Global.effort_preponderant==1  && Global.coeff_determination>0.9);
                    if(result)
                        commentaireEnCours="From the current rule the model " + RechercheModele.getEquation()+ " is convenient.";
                    break;
              case 7:
                    result = (Global.effort_preponderant!=1 && Global.environmental_influence.equals("abundance") && Global.cpue_sous_sur_production!=1 && Global.coeff_determination>0.7);
                    if(result)
                        commentaireEnCours="From the current rule the model " + RechercheModele.getEquation()+ " is convenient.";
                    break;
              case 8:
                    result = (Global.effort_preponderant!=1 && Global.environmental_influence.equals("abundance") && Global.cpue_sous_sur_production==1 && Global.coeff_determination>0.7);
                    if(result)
                        commentaireEnCours="From the current rule the model " + RechercheModele.getEquation()+ " is convenient.";
                    break;
              case 9:
                    result = (Global.effort_preponderant!=1  && Global.coeff_determination>0.9);
                    if(result)
                        commentaireEnCours="From the current rule the model " + RechercheModele.getEquation()+ " is convenient.";
                    break;
              case 2:
                    result=(Global.test_jackknife==false);
                    System.out.println("La valeur de test_jackknife est "+Global.test_jackknife);
                    if(result){
                      commentaireEnCours="The Student's test on jackknife is not acceptable.\nAt least one of the regression coefficients is not significant as you can see in the jackknife plots.\nFrom the current rule the model " + RechercheModele.getEquation()+ " is not validated";
                        
                    }
                    else
                      commentaireEnCours="The Student's test on jackknife regression coefficients is acceptable";
                    commentaireEnCours=commentaireEnCours+"\n\nDue to usual autocorrelation in time series and non independance between CPUE and E, conventional statistics are usually provide too optimistic results";
                    break;
              case 1:
                    result=( Global.coeff_determination_instable!=1);
                    if(result)
                      commentaireEnCours="From the current rule the model " + RechercheModele.getEquation()+ " is not validated";
                    break;
              case 0:
                    result=(Global.good_results!=1);
                    if(result)
                      commentaireEnCours="From the current rule the model " + RechercheModele.getEquation()+ " is not validated";
                    break;
              case 15:
                    result=(Global.trend_residuals!=1);
                    if(result)
                      commentaireEnCours="From the current rule the model " + RechercheModele.getEquation()+ " is not validated";
                    break;
             case 3:
                    result=true;
                    if(result)
                      commentaireEnCours="From the current rule the model " + RechercheModele.getEquation()+ " is  validated";
                    break;
            case 32:
                   result= (Global.environmental_influence.equals(Global.influenceEnv[2]));
                   break;
          }
          if(result && commentaireEnCours.equals(""))
              commentaireEnCours=(String)htComment.get(Integer.toString(numEnCours));
          System.out.println("LE commentaire est "+commentaireEnCours);
          System.out.println("On teste " + numEnCours + " elle est "+ result );
return result;
}

public static String getComment(){
    return commentaireEnCours;
}


}