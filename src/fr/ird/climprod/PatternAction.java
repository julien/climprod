/**
 * Titre : Climprod<p>
 *
 */
package fr.ird.climprod;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.Vector;
import javax.swing.*;

public class PatternAction extends AbstractAction {

    public PatternAction(Vector Series, JList listeSeries, PanelPlot panel, JComboBox cboLigne, SelecteurCouleur couleurLigne, JComboBox cboPoint, SelecteurCouleur couleurPoint) {
        pligne = couleurLigne;
        ppoint = couleurPoint;
        cbopoint = cboPoint;
        cboligne = cboLigne;
        target = panel;
        lst = listeSeries;
        series = Series;

    }

    @Override
    public void actionPerformed(ActionEvent evt) {

        double[] x = new double[2];
        double[] y = new double[2];
        x[0] = 0;
        x[1] = 3;
        y[0] = 0;//target.getHeight()/2;
        y[1] = 0;//target.getHeight()/2;
        PlotSerie ps = new PlotSerie(" ", x, " ", y);
        PlotModal p = new PlotModal();
        ps.setFigure(2);
        ps.setFigureLigne(cboligne.getSelectedIndex());
        ps.setMark(cbopoint.getSelectedIndex());
        ps.setCouleur(pligne.getSelectedColor());
        ps.setCouleurPoints(ppoint.getSelectedColor());
        p.setValeurs(ps);

        target.setPlot(p);
        target.update();
        PlotSerie s = (PlotSerie) series.get(lst.getSelectedIndex());
        //System.out.println("Clone de la s�rie dans Action " + s.toString());
        Object source = evt.getSource();

        if (source == cboligne) {
            s.setFigureLigne(cboligne.getSelectedIndex());
        } else if (source == cbopoint) {
            s.setMark(cbopoint.getSelectedIndex());
        } else if (source == pligne.cboColor) {
            s.setCouleur(pligne.getSelectedColor());
        } else if (source == ppoint.cboColor) {
            s.setCouleurPoints(ppoint.getSelectedColor());
        }

    }

    private final PanelPlot target;
    private final SelecteurCouleur pligne, ppoint;
    private Color colorLine, colorPoint;
    private final JComboBox cbopoint, cboligne;
    private final JList lst;
    private final Vector series;
}
