
/**
 * Titre : Climprod<p>
 */
package fr.ird.climprod;

import java.util.StringTokenizer;
import java.util.Hashtable;
import java.util.HashMap;
import java.io.InputStream;

public class RechercheModele {
    private static int nbData=0;
    private static int[] ordre;                //Ordre de s�lection des mod�les lors de la recherche
    private static String [] equation;         //Equation litterale
    private static String [] help;             //Fichier d'aide associ�
    private static int [] type;                //Effort-Climat-Mixte
    private static int [] affichage;           //Ordre d'affichage dans la liste
    private static String [] infl_Env;         //Influence de l'environnement
    private static int [] nbre_param;          //Nbre de param�tre du mod�le
    private static int [] forme1;              //Forme �quation sur param 1
    private static int [] numMod;              //Num�ro mod�le (pas utile!)
    private static int [] forme2;              //Forme �quation sur param 2
    private static boolean[] teste;            //Vrai si mod�le test�

    private static String[] listeModeleE;
    private static String[] listeModeleV;
    private static String[] listeModeleEVAbundance;
    private static String[] listeModeleEVCatchability;
    private static String[] listeModeleEVBoth;
    //private static Hashtable htNum=new Hashtable();

    private static int numEnCours;
    private static int indexEnCours;
    private static boolean foundModel=false;

    final static int lineaire=1;
    final static int exponentiel=2;
    final static int general=3;
    final static int power=4;
    final static int quadratique=5;

    final static int abondance=1;
    final static int prise=2;
    final static int both=3;

    final static int effort=1;
    final static int climat=2;
    final static int mixte=3;

 /*
  Assure la lecture du fichier des questions et initialise l'ensemble des variables.
  @param un String repr�sentant le nom "compl�t" du fichier listemodele.cvs.
*/

  public static void initScript(InputStream file)
 //  public static void initScript(String file)
  {
      try
      {
        HashMap<String, String> htTemp=new HashMap<String, String>();
        boolean doublon=false;
        String[] dataLine;

        ReadFileText rft=new ReadFileText(file);
        dataLine=rft.getLines();
        nbData=dataLine.length;

        String[] mE=new String[nbData];
        String[] mV=new String[nbData];
        String[] mEVA=new String[nbData];
        String[] mEVC=new String[nbData];
        String[] mEVB=new String[nbData];
        int ie=0,iv=0,ieva=0,ievc=0,ievb=0;
        equation=new String[nbData];
        help=new String[nbData];
        ordre=new int[nbData];
        type=new int[nbData];
        numMod=new int[nbData];
        affichage=new int[nbData];

        infl_Env=new String[nbData];
        nbre_param=new int[nbData];
        teste=new boolean[nbData];
        forme1=new int[nbData];
        forme2=new int[nbData];
        for (int i=0;i<dataLine.length;i++)
        {
             teste[i]=false;
             StringTokenizer d=new StringTokenizer (dataLine[i],";");
             int num=Integer.parseInt(d.nextToken().trim());
             ordre[num]=i;
             //htNum.put(d.nextToken().trim(),Integer.toString(i));
             numMod[i]=Integer.parseInt(d.nextToken().trim());
             type[i]=Integer.parseInt(d.nextToken().trim());
             affichage[i]=Integer.parseInt(d.nextToken().trim());
             equation[i]=(d.nextToken().trim());
             doublon=true;  //pour prendre en compte les �quations en doubles
             if(htTemp.get(equation[i]) ==null)
             {
                  htTemp.put(equation[i],Integer.toString(i));
                  doublon=false;
             }
             nbre_param[i]=Integer.parseInt(d.nextToken().trim());
             help[i]=(d.nextToken().trim().toUpperCase());
             forme1[i]=Integer.parseInt(d.nextToken().trim());
             forme2[i]=Integer.parseInt(d.nextToken().trim());
             int index=Integer.parseInt(d.nextToken().trim());
             //String[] influence={"","abundance","catchability","both"};
             infl_Env[i]=Global.influenceEnv[index];
             if(!doublon)
             {
             switch (index)
             {
                case 0:
                    if( type[i]==1)
                    {
                      mE[ie]=equation[i];
                      ie=ie+1;
                    }
                    else
                    {
                      mV[iv]=equation[i];
                      iv=iv+1;
                    }
                    break;
                case 1:
                    mEVC[ievc]=equation[i];
                    ievc=ievc+1;
                    break;
                case 2:
                    mEVA[ieva]=equation[i];
                    ieva=ieva+1;
                    break;
                case 3:
                    mEVB[ievb]=equation[i];
                    ievb=ievb+1;
                    break;
             }
            }
            listeModeleE=new String[ie];
            listeModeleV=new String[iv];
            listeModeleEVAbundance=new String[ieva];
            listeModeleEVCatchability=new String[ievc];
            listeModeleEVBoth=new String[ievb];
            for(int c=0;c<ie;c++)
                listeModeleE[c]=mE[c];
            for(int c=0;c<iv;c++)
                listeModeleV[c]=mV[c];
            for(int c=0;c<ieva;c++)
                listeModeleEVAbundance[c]=mEVA[c];
            for(int c=0;c<ievc;c++)
                listeModeleEVCatchability[c]=mEVC[c];
            for(int c=0;c<ievb;c++)
                listeModeleEVBoth[c]=mEVB[c];

             //System.out.println(ordre[num]+"  " +numMod[i]+"  "+  type[i]+"  "+affichage[i]+" "+equation[i]+" "+nbre_param[i]+"  "+help[i]+"  "+infl_Env[i]);

        }

      }
      catch(Exception e)
      {
         MsgDialogBox msg=new MsgDialogBox(0,"Invalid list model file: "+e.getMessage(),0, Global.CadreMain);
      }
}


/*
  S�lectionne un mod�le en fonction des parametres connus
*/
public static void select(){
int  newIndex=-1;
int i=0;
foundModel=false;
System.out.println("On recherche un modèle " + Global.relationCPU_E+" " +Global.relationCPU_V );
do
{
        int j=ordre[i];
       //System.out.println(type[j]+"  "+Global.typeModele+"***"+forme1[j]+"  "+Global.relationCPU_E+"***"+forme2[j]+"  "+Global.relationCPU_V+"***"+infl_Env[j]+"  "+(Global.environmental_influence));
        //System.out.println(forme1[j]+"  "+Global.relationCPU_E);
       // System.out.println(forme2[j]+"  "+Global.relationCPU_V);
        //System.out.println(infl_Env[j]+"  "+(Global.environmental_influence));
       // System.out.println("************************");
        if(teste[j]==false && type[j]==Global.typeModele && forme1[j]==Global.relationCPU_E && forme2[j]==Global.relationCPU_V && infl_Env[j].equals(Global.environmental_influence))
        {
              indexEnCours=j;//i;
              foundModel=true;
             // System.out.println("Equ "+equation[j]);

        }
        i=i+1;
       //System.out.println(foundModel+"  "+ i+"  "+nbData);
}
while(foundModel==false && i<nbData);
//System.out.println("On a trouv� un mod�le " + RechercheModele.getCpue_ERelation()+" " +RechercheModele.getCpue_VRelation() );
}

/*
  S�lectionne le mod�le correspondant � l'�quation pass�e en param�tre
  @param un String L'�quation du mod�le recherch�
*/
public static void select(String formule){
  foundModel=false;
//System.out.println("On recherche un mod�le " + Global.relationCPU_E+" " +Global.relationCPU_V );
  for (int i=0;i<nbData;i++)
  {
    if(formule.equals(equation[i]))
    {
          indexEnCours=i;
          foundModel=true;
          return;
    }
  }

}


/*
  Donne la forme de l'�quation
  @return : String la forme litrerale de la relation.
*/
public static String getEquation(){
      if(foundModel)
          return equation[indexEnCours];
      else
          return "no selected model";

}

/*
  Donne le rang d'affichage du mod�le
  @return : int la forme litrerale de la relation.
*/
public static int getNumero(){
      if(foundModel)
          return affichage[indexEnCours];
      else
          return -1;

}



/*
  Donne la forme de la relation entre la CPUE et V
  @return : String la forme litrerale de la relation.
*/

public static String getCpue_VRelation(){
      if(foundModel)
          switch(forme2[indexEnCours])
          {
                 case lineaire:
                    return "CPUE=a+b.V";
                 case general:
                    return "CPUE=a+b.V^c";
                 case power:
                    return "CPUE=a.V^b";
                 case quadratique:
                    return "CPUE=a.V+b.V^2+c";
                  default:
                    return "";
          }
      else
          return "no selected model";

}
/*
  Donne la forme de la relation entre la CPUE et V
  @return : int l'entier repr�sentant la forme de la relation.
*/

public static int  getNumCpue_VRelation(){
      if(foundModel)
          return(forme2[indexEnCours]);
       else
          return -1;

}
/*
Indique si un mod�le est s�lectionn�
@return boolean true si un mod�le est s�lectionn�
*/
public static boolean modelIsSelected(){
      return foundModel;
}

/*
  Donne la forme de la relation entre la CPUE et E
  @return : String la forme litterale de la relation.
*/

public static String getCpue_ERelation(){
      if(foundModel)
          switch(forme1[indexEnCours])
          {
                 case lineaire:
                    return "CPUE=a+b.E";
                 case general:
                    return "CPUE=(a+b.E)^(1/(c-1))";
                 case exponentiel:
                    return "CPUE=a.exp(b.E)";

                 default:
                    return "";
          }
      else
          return "no selected model";

}

/*
  Donne la forme de la relation entre la CPUE et E
  @return : int l'entier repr�sentant la forme de la relation.
*/

public static int getNumCpue_ERelation(){
      if(foundModel)
          return(forme1[indexEnCours]);
       else
          return -1;

}

public static int getType(){
  if(foundModel)
          return type[indexEnCours];
      else
          return -1;
}

public static String getModelInformation(){
  if(foundModel)
          return help[indexEnCours];
      else
          return "";
}




public static void reset(){
    foundModel=false;
    for(int i=0;i<nbData;i++)
          teste[i]=false;

}

/*
    Donne la liste des  mod�les correspondant au type pass� en param�tre
    @param  un enteir typeModele (0:cpue=f(e),1:cpue=f(v),2:cpue=f(e,v) abondance,3:cpue=f(e,v) prise,3:cpue=f(e,v) les deux
    @return un String[] la liste des mod�les ou null ;
*/

public static String[] getlisteModele(int typeModele){
  switch (typeModele)
  {
      case 0:
          return listeModeleE;
      case 1:
          return listeModeleV;
      case 2:
          return listeModeleEVAbundance;
      case 3:
          return listeModeleEVCatchability;
      case 4:
          return listeModeleEVBoth;
      default:
          return null;
   }


}
/*
 Donne l'influence de l'environnement pout le modele s�lectionn�
 @return un String ou null si pas de mod�le s�lectionn�.
*/
public static String getInfluenceEnvironment(){
 if(foundModel)
          return infl_Env[indexEnCours];
      else
          return null;
}
}