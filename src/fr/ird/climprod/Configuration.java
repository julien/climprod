/**
 * Titre : Climprod<p>
 *
 * Classe statique utilitaire qui g�re l'ensemble des pr�f�rences de
 * l'utilisateur quant � l'emplacement de ses r�pertoires de travail.
 */
package fr.ird.climprod;

import java.io.*;
import java.util.StringTokenizer;
import java.util.Hashtable;
import java.util.HashMap;

public class Configuration {

    static String workingDirectory = "";
    static String applicationPathName = "";
    static String reglePathName = "";
    static String arbrePathName = "";
    static String commentPathName = "";
    static String modelePathName = "";
    static String helpPathName = "";
    static String userDir = "";
    static String userHome = "";
    static String userName = "";
    static String fileSep = "";
    static String lastPathSource = "";
    static String fileName = "";

    static {

        fileSep = System.getProperty("file.separator");
        if (fileSep.equals("/")) {
            fileSep = "//";
        }

        userName = System.getProperty("user.name");
        fileName = "Climprod" + userName.replace(' ', '_') + ".txt";
        File pref = new File(fileName);
        if (pref.exists()) {
            try {
                ReadFileText fl = new ReadFileText(fileName);
                String[] fldat = fl.getLines();
                HashMap<String, String> ht = new HashMap<String, String>();
                for (int i = 0; i < fldat.length; i++) {
                    String key = "";
                    String value = "";
                    StringTokenizer d = new StringTokenizer(fldat[i], "|");
                    //System.out.println(d.countTokens());
                    if (d.countTokens() == 2) {
                        ht.put(d.nextToken(), d.nextToken());
                    } else {
                        ht.put(d.nextToken(), " ");
                    }
                }
                workingDirectory = (String) ht.get("workingDirectory");
                applicationPathName = (String) ht.get("applicationPathName");
                reglePathName = (String) ht.get("reglePathName");
                arbrePathName = (String) ht.get("arbrePathName");
                commentPathName = (String) ht.get("commentPathName");
                lastPathSource = (String) ht.get("lastPathSource");
                modelePathName = (String) ht.get("modelePathName");
                helpPathName = (String) ht.get("helpPathName");
                userDir = (String) ht.get("userDir");
                userHome = (String) ht.get("userHome");
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        } else //n'existe pas on le renseigne et on le cr�e
        {
            //Recherche directory application
            StringTokenizer d = new StringTokenizer(System.getProperty("java.class.path"), System.getProperty("path.separator"));
            String dd = d.nextToken();

            int pos = dd.lastIndexOf("Climprod");
            applicationPathName = dd.substring(0, pos + 8);
            System.out.println("path " + applicationPathName);
            reglePathName = applicationPathName + fileSep + "Regles.csv";
            arbrePathName = applicationPathName + fileSep + "Arbre_decisions.csv";
            commentPathName = applicationPathName + fileSep + "Comment.csv";
            modelePathName = applicationPathName + fileSep + "ListeModele.csv";
            helpPathName = applicationPathName + fileSep + "help";
            File hd = new File(helpPathName);
            hd.mkdir();
            userDir = System.getProperty("user.dir");
            userHome = System.getProperty("user.home");
            workingDirectory = userHome + fileSep + ".climprod";
            File wd = new File(workingDirectory);
            wd.mkdir();
            lastPathSource = workingDirectory;
            try {
                SaveConfig();
            } catch (Exception ew) {
            }
        }
    }

    static public void SaveConfig() throws IOException {
        PrintWriter out = new PrintWriter(new FileWriter(fileName));
        out.println("workingDirectory|" + workingDirectory);
        out.println("applicationPathName|" + applicationPathName);
        out.println("reglePathName|" + reglePathName);
        out.println("arbrePathName|" + arbrePathName);
        out.println("modelePathName|" + modelePathName);
        out.println("lastPathSource|" + lastPathSource);
        out.println("userDir|" + userDir);
        out.println("userHome|" + userHome);
        out.println("commentPathName|" + commentPathName);
        out.println("helpPathName|" + helpPathName);
        out.close();
    }

}
