/**
 * Titre : Climprod<p>
 * Description :
 * <p>
 * Copyright : Copyright (c) T. L<p>
 * Soci�t� :
 * <p>
 * @author T. L
 * @version 1.0
 */
package fr.ird.climprod;

public class EquationModele {

    /*
     * Calcul de la moyenne sur un tableau d'entiers i.
     * @param int[]i.
     * @return un double, la moyenne.
     */
    static public double fonction_modele(double ff, double vv, double vba, double par[]) {
        double puec = 0;
        double tmp, tmp1, tmp2;
        int numeromod = Global.numero_modele;
        //System.out.println("numero" + numeromod);
        switch (numeromod) {
            case 0:
                puec = par[0] + par[1] * ff;
                break;
            case 1:
                puec = par[0] * Math.exp(par[1] * ff);
                break;
            case 2:
                puec = par[0] + par[1] * vba;
                break;
            case 3:
                puec = par[0] * Math.pow(vba, par[1]);
                break;
            case 4:
                puec = par[0] + par[1] * Math.pow(vba, par[2]);
                break;
            case 5:
                puec = par[0] * vba + par[1] * (vba * vba) + par[2];
                break;
            case 6:
                tmp = par[0] + par[1] * ff;
                if (tmp > 0) {
                    puec = Math.pow(tmp, 1.0 / (par[2] - 1));
                } else {
                    puec = -Math.pow(-tmp, 1.0 / (par[2] - 1));
                }
                break;
            case 7:
                puec = par[0] * Math.pow(vba, par[1]) + par[2] * ff;
                break;
            case 8:
                puec = par[0] + par[1] * vba + par[2] * ff;
                break;
            case 9:
                puec = par[0] * vba + par[1] * ff;
                break;
            case 10:
                puec = par[0] + par[1] * vba + par[2] * (vba * vba) + par[3] * ff;
                break;
            case 11:
                puec = par[0] * Math.pow(vba, par[1]) * Math.exp(par[2] * ff);
                break;
            case 12:
                puec = (par[0] + par[1] * vba) * Math.exp(par[2] * ff);
                break;
            case 13:
                puec = par[0] * vba * Math.exp(par[1] * ff);
                break;
            case 14:
                puec = par[0] + par[1] * vv - par[2] * (par[0] + par[1] * vv) * (par[0] + par[1] * vba) * ff;
                break;
            case 15:
                tmp1 = Math.pow(vv, par[1]);
                tmp2 = Math.pow(vba, par[1]);
                puec = par[0] * tmp1 + par[2] * tmp1 * tmp2 * ff;
                break;
            case 16:
                puec = par[0] * vv + par[1] * vv * vba * ff;
                break;
            case 17:
                tmp1 = Math.pow(vv, par[1]);
                tmp2 = Math.pow(vba, par[1]);
                puec = par[0] * tmp1 * Math.exp(par[2] * tmp2 * ff);
                break;
            case 18:
                puec = (par[0] + par[1] * vv)
                        * Math.exp(-par[2] * (par[0] + par[1] * vba) * ff);
                break;
            case 19:
                puec = par[0] * vv * Math.exp(par[1] * vba * ff);
                break;
            case 20:
                puec = par[0] * Math.exp(par[1] * ff) + par[2] * vba + par[3];
                break;
            case 21:
                puec = (par[0] * vba + par[1] * (vba * vba)) * Math.exp(par[2] * ff);
                break;
            case 22:
                tmp = par[0] * Math.pow(vba, par[1]) + par[2] * ff;
                if (tmp > 0) {
                    puec = Math.pow(tmp, 1.0 / (par[3] - 1));
                } else {
                    puec = -Math.pow(-tmp, 1.0 / (par[3] - 1));
                }
                break;
            case 23:
                tmp = par[0] * vba + par[1] * (vba * vba);
                if (tmp > 0) {
                    tmp = Math.pow(tmp, par[3] - 1) + par[2] * ff;
                } else {
                    tmp = -Math.pow(-tmp, par[3] - 1) + par[2] * ff;
                }
                if (tmp > 0) {
                    puec = Math.pow(tmp, 1. / (par[3] - 1));
                } else {
                    puec = -Math.pow(-tmp, 1. / (par[3] - 1));
                }
                break;
            case 24:
                tmp = par[1] - par[2] * vv;
                puec = par[0] * vv * tmp
                        - par[3] * vv * vba * tmp * (par[1] - par[2] * vba) * ff;
                break;
            case 25:
                puec = par[0] * vv * (1 + par[1] * vv) * Math.exp(par[2] * vba * (1 + par[1] * vba) * ff);
                break;
            case 26:
                tmp1 = Math.sqrt(vba * vv);
                puec = par[0] * Math.pow(tmp1, par[1] + par[2]) + par[3] * Math.pow(vba, 2 * par[1]) * ff;
                break;
            case 27:
                tmp1 = Math.pow(vv, par[1]);
                tmp2 = Math.pow(vba, par[1]);
                puec = par[0] * tmp1 * vba + par[2] * (vba * vba) * tmp1 + par[3] * tmp1 * tmp2 * ff;
                break;
            case 28:
                puec = par[0] * Math.pow(vba, par[1]) * Math.exp(par[2] * Math.pow(vba, par[3]) * ff);
                break;
            case 29:
                tmp1 = Math.sqrt(vba * vv);
                puec = par[0] * Math.pow(tmp1, par[1]) * Math.exp(par[2] * Math.pow(vba, par[3]) * ff);
                break;
            case 30:
                tmp1 = Math.pow(vv, par[1]);
                tmp2 = Math.pow(vba, par[1]);
                puec = (par[1] * tmp1 * vba + par[0] * tmp1 * (vba * vba)) * Math.exp(par[3] * tmp2 * ff);
                break;
        }
        return (puec);
    }

    static public double minimum_fonction(double vv, double par[]) {
        double ff = 0, aa, bb, cc;
        int numeromod = Global.numero_modele;
        switch (numeromod) {
            case 0:
                ff = -0.5 * par[0] / par[1];
                break;
            case 1:
                ff = -1.0 / par[1];
                break;
            case 2:
                ff = 0;
                break;
            case 3:
                ff = 0;
                break;
            case 4:
                ff = 0;
                break;
            case 5:
                ff = 0;
                break;
            case 6:
                ff = -par[0] * (par[2] - 1) / (par[1] * par[2]);
                break;
            case 7:
                ff = -0.5 * par[0] * Math.pow(vv, par[1]) / par[2];
                break;
            case 8:
                ff = -0.5 * (par[0] + par[1] * vv) / par[2];
                break;
            case 9:
                ff = -0.5 * par[0] * vv / par[1];
                break;
            case 10:
                ff = -0.5 * (par[0] + par[1] * vv + par[2] * (vv * vv)) / par[3];
                break;
            case 11:
                ff = -1.0 / par[2];
                break;
            case 12:
                ff = -1.0 / par[2];
                break;
            case 13:
                ff = -1.0 / par[1];
                break;
            case 14:
                ff = 0.5 / (par[2] * (par[0] + par[1] * vv));
                break;
            case 15:
                ff = -0.5 * par[0] * Math.pow(vv, par[1]) / (par[2] * Math.pow(vv, par[1] * 2));
                break;
            case 16:
                ff = -0.5 * par[0] / (par[1] * vv);
                break;
            case 17:
                ff = -1.0 / (par[2] * Math.pow(vv, par[1]));
                break;
            case 18:
                ff = 1.0 / (par[2] * (par[0] + par[1] * vv));
                break;
            case 19:
                ff = -1.0 / (par[1] * vv);
                break;
            case 20:
                ff = 0.0;
                /*
                 aa = par[1];
                 bb = par[2];
                 cc = par[3]*vv+par[4];
                 ff = minimis2(f0,aa,bb,cc);
                 */
                break;
            case 21:
                ff = -1.0 / par[2];
                break;
            case 22:
                ff = -(par[0] * Math.pow(vv, par[1])) * (par[3] - 1) / (par[3] * par[2]);
                break;
            case 23:
                ff = -Math.pow(par[0] * vv + par[1] * (vv * vv), par[3] - 1) * (par[3] - 1) / (par[3] * par[2]);
                break;
            case 24:
                ff = 0.5 * par[0] / (par[3] * vv * (par[1] - par[2] * vv));
                break;
            case 25:
                ff = -1.0 / (par[2] * vv * (1 + par[1] * vv));
                break;
            case 26:
                ff = -0.5 * par[0] * Math.pow(vv, par[2] - par[1]) / par[3];
                break;
            case 27:
                ff = -0.5 * (par[0] + par[2] * vv) / (par[3] * Math.pow(vv, par[1]));
                break;
            case 28:
                ff = -1.0 / (par[2] * Math.pow(vv, par[3]));
                break;
            case 29:
                ff = -1.0 / (par[2] * Math.pow(vv, par[3]));
                break;
            case 30:
                ff = -1.0 / (par[3] * Math.pow(vv, par[1]));
                break;
        }
        return (ff);
    }

}
