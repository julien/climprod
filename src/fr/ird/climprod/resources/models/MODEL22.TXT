MODEL: CPUE = (aV + b V^2) exp(c E)

This is a production model combining on the one hand an exponential 
relationship between the CPUE and the fishing effort E, and on the
other a quadratic (parabola) relationship between the CPUE and 
the environmental variable V. This latter variable is supposed to
only influence the stock abundance (surplus production) and not the
catchability coefficient.

This kind of non-monotonic equation is useful when the relationship between 
CPUE and V is shaped (optimal central value). The graph MSY versus V must 
OBVIOUSLY be parabolic. If not, the data-set does not justify such a model
and it is recommanded to use another model (CPUE = aV^b exp(cE) or CPUE =
(a+bV) exp(cE) for instance). 

Moreover, when  using these  formulae in a  non-equilibrium  condition
(transitional state) the user must  carefully check the temporal stability 
of the V variable  on the  time-series graph. In particular, it would be
nonsense to allow the program to average two V values with one located
on the left side of the parabola and the other on the right.

 

