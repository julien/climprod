MODEL: CPUE = a V exp(b V E)

This is a production model combining on the one hand an exponential 
relationship between the CPUE and the fishing effort E, and on the
other a linear relationship between the CPUE and the environmental 
variable V. This latter variable is supposed to only influence the
catchability coefficient and not the stock abundance (surplus 
production). 

The particularity of this model is the absence of a constant term in its
equation. After fitting this model, if the fit is not good it is suggested 
to try the following model which has a constant term but fewer degrees of 
freedom: 

CPUE = (a + b V) exp(-(c/a)(a + b V E).
 
  
  
 

