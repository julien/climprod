MODEL: CPUE = a + bV + cV^2 + dE

This is a production model combining on the one hand a linear relation-
ship between the CPUE and the fishing effort E, and on the other a
quadratic (parabola) relationship between the CPUE and the environmental
variable V. This latter variable is supposed to only influence the abundance
of the stock (surplus production) and not the catchability coefficient.

This kind of non-monotonic equation is useful when the relationship between 
CPUE and V is shaped (optimal central value). The graph MSY versus V must 
OBVIOUSLY be parabolic. If not, the data-set does not justify such a model
and it is recommanded to use a model with less parameters and more degree
of freedom (CPUE = a+bV+cE or CPUE = aV^b+cE for instance). 

Moreover, when  using these  formulae in a non-equilibrium  condition
(transitional state) the user must  carefully check the temporal stability 
of the V variable  on the  time-series graph. In particular, it would be
nonsense to allow the program to average two V values with one located
on the left side of the parabola and the other on the right.

If the fit is not good, it is suggested to fit a  model  where the
more flexible Ricker type equation is used to describe the relation-
ship between CPUE and V: CPUE = a V exp(bV) + c E. Unfortunately, these
equations are not available in the present version of CLIMPROD.
