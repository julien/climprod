May the stock present large fluctuations in CPUE when overexploited?

If you answer YES, the program will try to fit a particular model which
supposes that the relationship between fishing effort and CPUE is
exponential and combines the environmental and the effort effects in 
an additive way.

At the moment, only one additive model is available in the software. It
combines an exponential model for the relationship between CPUE and effort
(E) and a linear model for the relationship between CPUE and environment (V)
when the environment influences the stock abundance:

CPUE = a exp(bE) + cV + d

This model is supposed to be more suitable than the equivalent multiplicative
model when a high variability in the CPUE is observed and when the effort is
higher than the effort corresponding to the MSY. This situation may be
observed (but not necessarily) when the environmental influence occurs
before recruitment.

Answer NO to this question if you are not sure to be in this particular
situation.
